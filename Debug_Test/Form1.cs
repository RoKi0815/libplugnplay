﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Management;
using libPlugNPlay;

namespace Debug_Test
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //libPlugNPlay.WMI_Info.GetWMIClassNames();
            
            //var Devices = w32_PnPDevice.GetPnPDevices();
            var Devices = Win32_PnPDevice.GetPnPDevices();

            //List<Win32_PnPEntity> Devices = new List<Win32_PnPEntity>();
            //foreach (ManagementObject Device in new ManagementObjectSearcher(@"SELECT * FROM Win32_PhysicalMedia").Get())
            //{
            //    Devices.Add(new Win32_PnPEntity(Device));
            //}

            this.listBox1.Items.Clear();

            foreach (var Device in Devices)
            {
                this.listBox1.Items.Add(Device.SystemElement.Name);
                //foreach (ManagementObject mbo in new ManagementObjectSearcher("SELECT * FROM Win32_PnPEntity WHERE ClassGuid = '"+Device.ClassGuid+"'").Get())
                //{
                //    Console.WriteLine("ClassName : " + mbo.GetPropertyValue("Name"));
                //    break;
                //}
                Console.WriteLine("Name : " + Device.SystemElement.Name);
                Console.WriteLine("PnPClass : " + Device.SystemElement.PNPClass);
                Console.WriteLine("Class : " + Device.SystemElement.GetType().Name.ToString());
                Console.WriteLine("ClassGUID : " + Device.SystemElement.ClassGuid);
                Console.WriteLine("PnPDeviceID : " + Device.SystemElement.PNPDeviceID);
                Console.WriteLine("DeviceID : " + Device.SystemElement.DeviceID);
                Console.WriteLine("CreationClassNAme : " + Device.SystemElement.CreationClassName);
                Console.WriteLine("SystemCreationClassName : " + Device.SystemElement.SystemCreationClassName);
                if (Device.GetType().Equals(typeof(Win32_DiskDrive)))
                {
                    Console.WriteLine("Serialnumber: " + ((Win32_DiskDrive)Device.SystemElement).SerialNumber.ToString());
                    Console.WriteLine("Interface: " + ((Win32_DiskDrive)Device.SystemElement).InterfaceType.ToString());
                }
                if (Device.GetType().Equals(typeof(Win32_CDROMDrive)))
                {
                    Console.WriteLine("Serialnumber: " + ((Win32_CDROMDrive)Device.SystemElement).SerialNumber.ToString());
                    Console.WriteLine("MediaLoaded: " + ((Win32_CDROMDrive)Device.SystemElement).MediaLoaded.ToString());
                }
            }
        }
    }
}
