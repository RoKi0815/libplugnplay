﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace libPlugNPlay
{
    #region WMI Attributes
    [System.AttributeUsage(System.AttributeTargets.Property)]
    public class WMIPropertyAttribute : System.Attribute
    {
        public WMIPropertyAttribute()
        { }
    }

    [System.AttributeUsage(System.AttributeTargets.Method)]
    public class WMIMethodAttribute : System.Attribute
    {
        public WMIMethodAttribute()
        { }
    }

    [System.AttributeUsage(System.AttributeTargets.Property | System.AttributeTargets.Method)]
    public class WMINotExistingAttribute : System.Attribute
    {
        public WMINotExistingAttribute()
        { }
    }
    #endregion

    #region Extensions
    public static class AttributeExtension
    {
        public static bool HasAttribute<T>(this System.Reflection.ICustomAttributeProvider provider) where T : Attribute
        {
            var atts = provider.GetCustomAttributes(typeof(T), true);
            return atts.Length > 0;
        }
    }

    public static class TypeExtension
    {
        public static bool IsNullableEnum(this Type t)
        {
            Type u = Nullable.GetUnderlyingType(t);
            return (u != null) && u.IsEnum;
        }
    }
    #endregion

    #region HelperClasses
    /// <summary>
    /// zerlegt einen Object Path in seine Elemente
    /// </summary>
    public struct WMIObjectReference
    {
        public string WMI_Namespace;
        public string WMI_Class;
        public string WMI_KeyProperties_string;
        public KeyProperty[] WMI_KeyProperties;

        /// <summary>
        /// Konstruktor. Erstellt ein Objekt mit Hilfe von eines Voll qualifizierten Object Path.
        /// </summary>
        /// <param name="WMIObjectReferenceString">voll Qualifizierter Object Path</param>
        public WMIObjectReference(string WMIObjectReferenceString)
        {
            this.WMI_Namespace = WMIObjectReferenceString.Split(new[] { ':' }, 2)[0];
            this.WMI_Class = WMIObjectReferenceString.Replace(this.WMI_Namespace + ":", "").Split(new[] { '.' }, 2)[0];
            this.WMI_KeyProperties_string = WMIObjectReferenceString.Replace(this.WMI_Namespace + ":" + this.WMI_Class + ".", "");
            System.Collections.Generic.List<KeyProperty> props = new System.Collections.Generic.List<KeyProperty>();
            foreach (string prop in this.WMI_KeyProperties_string.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                props.Add(new KeyProperty(prop));
            }
            this.WMI_KeyProperties = props.ToArray();
        }

        /// <summary>
        /// Gibt den Klassenpfad der Objektreferenz zals Zeichenkette zurück.
        /// </summary>
        /// <returns>Klassenpfad der Objektreferenz</returns>
        public string ToManagementPathString()
        {
            return this.ToManagementPath().Path;
        }

        /// <summary>
        /// Gibt den Klassnepfad der Objektreferenz als ManagementPath-Objekt zurück.
        /// </summary>
        /// <returns>ManagementPath-Objekt der Objektreferenz</returns>
        public System.Management.ManagementPath ToManagementPath()
        {
            return new System.Management.ManagementPath(this.WMI_Namespace + ":" + this.WMI_Class);
        }

        /// <summary>
        /// Gibt die Klasse der Objektreferenz als ManagementClass-Objekt zurück.
        /// </summary>
        /// <returns></returns>
        public System.Management.ManagementClass ToManagementClass()
        {
            return new System.Management.ManagementClass(this.ToManagementPath());
        }

        /// <summary>
        /// Gibt die Klasse der Objektreferenz als WMIClassReference-Objekt zurück.
        /// </summary>
        /// <returns></returns>
        public WMIClassReference ToClassReference()
        {
            return new WMIClassReference(this.ToManagementClass());
        }

        /// <summary>
        /// Gibt den Typ der Klasse der Objektreferenz zurück.
        /// </summary>
        /// <returns>Objektinstanz der Objektreferenz</returns>
        public System.Type GetWMIClassType()
        {
            return this.ToClassReference().WMI_ClassType;
        }

        /// <summary>
        /// Gibt das Objekt der Objektreferenz als ManagementBasObject zurück.
        /// </summary>
        /// <returns>Objektinstanz der Objektreferenz</returns>
        public System.Management.ManagementBaseObject GetWMIObject()
        {
            foreach(System.Management.ManagementBaseObject InstanceObject in new System.Management.ManagementObjectSearcher("SELECT * FROM " + this.WMI_Class + " WHERE " + this.WMI_KeyProperties_string).Get())
            {
                return InstanceObject;
            }
            return null;
        }
    }

    public struct KeyProperty
    {
        public string Name;
        public string Value;

        public KeyProperty(string KeyPropertyPair)
        {
            this.Name = KeyPropertyPair.Split(new[] { '=' }, 2)[0];
            this.Value = KeyPropertyPair.Split(new[] { '=' }, 2)[1];
        }

        public KeyProperty(string Name, string Value)
            : this(Name + "=" + Value)
        { }
    }

    public struct WMIClassReference
    {
        public string WMI_Namespace;
        public string WMI_Class;
        public readonly System.Type WMI_ClassType;
        /// <summary>
        /// Array of all supported WMI ClassTypes
        /// </summary>
        public static readonly System.Collections.Generic.Dictionary<System.Management.ManagementClass, System.Type> SupportedTypes = new Dictionary<System.Management.ManagementClass, Type>();

        static WMIClassReference()
        {
            WMIClassReference.SupportedTypes.Add(Win32_PnPEntity.WMI_ClassReference, typeof(Win32_PnPEntity));
            WMIClassReference.SupportedTypes.Add(Win32_PhysicalMedia.WMI_ClassReference, typeof(Win32_PhysicalMedia));
            WMIClassReference.SupportedTypes.Add(Win32_DiskDrive.WMI_ClassReference, typeof(Win32_DiskDrive));
            WMIClassReference.SupportedTypes.Add(Win32_CDROMDrive.WMI_ClassReference, typeof(Win32_CDROMDrive));
            WMIClassReference.SupportedTypes.Add(Win32_FloppyDrive.WMI_ClassReference, typeof(Win32_FloppyDrive));
            WMIClassReference.SupportedTypes.Add(Win32_TapeDrive.WMI_ClassReference, typeof(Win32_TapeDrive));
        }

        public WMIClassReference(System.Management.ManagementClass ClassDefinition)
        {
            if (WMIClassReference.SupportedTypes.ContainsKey(ClassDefinition))
            {
                this.WMI_ClassType = WMIClassReference.SupportedTypes[ClassDefinition];
            }
            else
            {
                this.WMI_ClassType = typeof(Win32_PnPEntity);
            }
            this.WMI_Namespace = ClassDefinition.ClassPath.Path.Split(new[] { ':' }, 2)[0];
            this.WMI_Class = ClassDefinition.ClassPath.Path.Replace(this.WMI_Namespace + ":", "").Split(new[] { '.' }, 2)[0];
        }

        /// <summary>
        /// Gibt den Klassnepfad der Objektreferenz als ManagementPath-Objekt zurück.
        /// </summary>
        /// <returns>ManagementPath-Objekt der Objektreferenz</returns>
        public System.Management.ManagementPath ToManagementPath()
        {
            return new System.Management.ManagementPath(this.WMI_Namespace + ":" + this.WMI_Class);
        }

        /// <summary>
        /// Gibt die Klasse der Objektreferenz als ManagementClass-Objekt zurück.
        /// </summary>
        /// <returns></returns>
        public System.Management.ManagementClass ToManagementClass()
        {
            return new System.Management.ManagementClass(this.ToManagementPath());
        }
    }
    #endregion

    public static class WMI
    {
        public static T GetTypedObject<T>(System.Management.ManagementObject device) where T : Win32_PnPEntity, new()
        {
            try
            {
                return (T)typeof(T).GetConstructor(new System.Type[1] { typeof(System.Management.ManagementObject) }).Invoke(new System.Management.ManagementObject[1] { device });
            }
            catch (System.Exception ex)
            {
                if (ex is System.Reflection.AmbiguousMatchException || ex is System.Reflection.TargetInvocationException || ex is System.Reflection.TargetException)
                {
                    return null;
                }
                else
                {
                    throw;
                }
            }
        }
    }
    
    public static class WMI_Info
    {
        public static string[] GetWMIClassNames()
        {
            foreach (System.Management.ManagementBaseObject mbo in new System.Management.ManagementObjectSearcher("SELECT * FROM meta_class").Get())
            {
                System.Management.PropertyDataCollection pdc = mbo.Properties;
                Console.WriteLine(pdc["Name"].Value);
            }
            return new string[0] { };
        }
    }
}
