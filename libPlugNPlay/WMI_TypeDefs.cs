﻿namespace libPlugNPlay
{
    /// <summary>
    /// Enumeration for the Availability of Win32_PnPEntity
    /// </summary>
    public enum enumAvailability : ushort
    {
        /// <summary>
        /// Other = 1(0x01)
        /// </summary>
        Other = 0x01,
        /// <summary>
        /// Unknown = 2(0x02)
        /// </summary>
        Unknown = 0x02,
        /// <summary>
        /// Running or Full Power = 3(0x03)
        /// </summary>
        Running = 0x03,
        /// <summary>
        /// Running or Full Power = 3(0x03)
        /// </summary>
        FullPower = 0x03,
        /// <summary>
        /// Warning = 4(0x04)
        /// </summary>
        Warning = 0x04,
        /// <summary>
        /// In Test = 5(0x05)
        /// </summary>
        Test = 0x05,
        /// <summary>
        /// Not Applicable = 6(0x06)
        /// </summary>
        NotApplicable = 0x06,
        /// <summary>
        /// Power Off = 7(0x7)
        /// </summary>
        PowerOff = 0x07,
        /// <summary>
        /// Off Line = 8(0x08)
        /// </summary>
        OffLine = 0x08,
        /// <summary>
        /// Off Duty = 9(0x09)
        /// </summary>
        OffDuty = 0x09,
        /// <summary>
        /// Degraded = 10(0x0A)
        /// </summary>
        Degraded = 0x0A,
        /// <summary>
        /// Not Installed = 11(0x0B)
        /// </summary>
        NotInstalled = 0x0B,
        /// <summary>
        /// Install Error = 12(0x0C)
        /// </summary>
        InstallError = 0x0C,
        /// <summary>
        /// Power Save - Unknown = 13(0x0D)
        /// The device is known to be in a power save mode, but its exact status is unknown.
        /// </summary>
        PowerSave_Unknown = 0x0D,
        /// <summary>
        /// Power Save - Low Power Mode = 14(0x0E)
        /// The device is in a power save state but still functioning, and may exhibit degraded performance.
        /// </summary>
        PowerSave_LowPowerMode = 0x0E,
        /// <summary>
        /// Power Save - Standby = 15(0x0F)
        /// The device is not functioning, but could be brought to full power quickly.
        /// </summary>
        PowerSave_Standby = 0x0F,
        /// <summary>
        /// Power Cycle = 16(0x10)
        /// </summary>
        PowerCycle = 0x10,
        /// <summary>
        /// Power Save - Warning = 17(0x11)
        /// The device is in a warning state, though also in a power save mode.
        /// </summary>
        PowerSave_Warning = 0x11
    }

    /// <summary>
    /// Enumeration for the StatusInfo of Win32_PnPEntity
    /// </summary>
    public enum enumStatusInfo : ushort
    {
        /// <summary>
        /// Other = 1
        /// </summary>
        Other = 1,
        /// <summary>
        /// Unknown = 2
        /// </summary>
        Unknown = 2,
        /// <summary>
        /// Enabled = 3
        /// </summary>
        Enabled = 3,
        /// <summary>
        /// Disabled = 4
        /// </summary>
        Disabled = 4,
        /// <summary>
        /// Not Applicable = 5
        /// </summary>
        NotApplicable = 5
    }

    /// <summary>
    /// Dictionary for Status of Win32_PnPEntity
    /// </summary>
    public static class dictionaryStatus
    {
        public const string OK = "OK";
        public const string Error = "Error";
        public const string Degraded = "Degraded";
        public const string Unknown = "Unknown";
        public const string PredFail = "Pred Fail";
        public const string Starting = "Starting";
        public const string Stopping = "Stopping";
        public const string Service = "Service";
        public const string NonRecover = "NonRecover";
        public const string NoContact = "No Contact";
        public const string LostComm = "Lost Comm";
    }

    /// <summary>
    /// enumeration for Capabilities of Win32_DiskDrive/Win32_CDROMDrive
    /// </summary>
    public enum emunCapabilities : ushort
    {
        /// <summary>
        /// Unknown (0)
        /// </summary>
        Unknown = 0,
        /// <summary>
        /// Other (1)
        /// </summary>
        Other = 1,
        /// <summary>
        /// Sequential Access (2)
        /// </summary>
        SequentialAccess = 2,
        /// <summary>
        /// Random Access
        /// </summary>
        RandomAccess = 3,
        /// <summary>
        /// Supports Writing
        /// </summary>
        SupportsWriting = 4,
        /// <summary>
        /// Encryption
        /// </summary>
        Encryption = 5,
        /// <summary>
        /// Compression
        /// </summary>
        Compression = 6,
        /// <summary>
        /// Supports Removable Media
        /// </summary>
        SupportsRemovableMedia = 7,
        /// <summary>
        /// Manual Cleaning
        /// </summary>
        ManualCleaning = 8,
        /// <summary>
        /// Automatic Cleaning
        /// </summary>
        AutomaticCleaning = 9,
        /// <summary>
        /// SMART Notification
        /// </summary>
        SMARTNotification = 10,
        /// <summary>
        /// Supports Dual-Sided Media
        /// </summary>
        SupportsDualSidedMedia = 11,
        /// <summary>
        /// Ejection Prior to Drive Dismount Not Required
        /// </summary>
        EjectionPriorToDriveDismountNotRequired = 12
    }

    /// <summary>
    /// dictionary for CompressionMethod of Win32_DiskDrive/Win32_CDROMDrive
    /// </summary>
    public static class dictionaryCompressionMethod
    {
        /// <summary>
        /// Whether the device supports compression capabilities or not is not known.
        /// "Unknown" to represent that it is unknown whether the device supports compression capabilities.
        /// </summary>
        public const string Unknown = "Unknown";
        /// <summary>
        /// The device supports compression capabilities but its compression scheme is not known or not disclosed.
        /// "Compressed" to represent that the device supports compression capabilities, but its compression scheme is either unknown or undisclosed.
        /// </summary>
        public const string Compressed = "Compressed";
        /// <summary>
        /// The device does not support compression.
        /// "Not Compressed" to represent that the devices does not support compression capabilities.
        /// </summary>
        public const string NotCompressed = "Not Compressed";
    }

    /// <summary>
    /// enumeration for ConfigManagerErrorCode of Win32_DiskDrive
    /// </summary>
    public enum enumConfigManagerErrorCode : uint
    {
        /// <summary>
        /// Device is working properly. 0 (0x0)
        /// </summary>
        DeviceOK = 0x0,
        /// <summary>
        /// Device is not configured correctly. 1 (0x1)
        /// </summary>
        DeviceConfigError = 0x1,
        /// <summary>
        /// Windows cannot load the driver for this device. 2 (0x2)
        /// </summary>
        LoadDriverError = 0x2,
        /// <summary>
        /// Driver for this device might be corrupted, or the system may be low on memory or other resources. 3 (0x3)
        /// </summary>
        LowRessourcesError = 0x3,
        /// <summary>
        /// Device is not working properly. One of its drivers or the registry might be corrupted. 4 (0x4)
        /// </summary>
        CorruptDriverError = 0x4,
        /// <summary>
        /// Driver for the device requires a resource that Windows cannot manage. 5 (0x5)
        /// </summary>
        DriverRessourceError = 0x5,
        /// <summary>
        /// Boot configuration for the device conflicts with other devices. 6 (0x6)
        /// </summary>
        BootConfigError = 0x6,
        /// <summary>
        /// Cannot filter. 7 (0x7)
        /// </summary>
        FilterError = 0x7,
        /// <summary>
        /// Driver loader for the device is missing. 8 (0x8)
        /// </summary>
        MissingDriverLoaderError = 0x8,
        /// <summary>
        /// Device is not working properly. The controlling firmware is incorrectly reporting the resources for the device. 9 (0x9)
        /// </summary>
        FirmwareRessourcesError = 0x9,
        /// <summary>
        /// Device cannot start. 10 (0xA)
        /// </summary>
        DeviceStartError = 0xA,
        /// <summary>
        /// Device failed. 11 (0xB)
        /// </summary>
        DeviceFailedError = 0xB,
        /// <summary>
        /// Device cannot find enough free resources to use. 12 (0xC)
        /// </summary>
        NoFreeRessourceError = 0xC,
        /// <summary>
        /// Windows cannot verify the device's resources. 13 (0xD)
        /// </summary>
        RessourceVerifyError = 0xD,
        /// <summary>
        /// Device cannot work properly until the computer is restarted. 14 (0xE)
        /// </summary>
        RestartRequiredError = 0xE,
        /// <summary>
        /// Device is not working properly due to a possible re-enumeration problem. 15 (0xF)
        /// </summary>
        ReEnumerationError = 0xF,
        /// <summary>
        /// Windows cannot identify all of the resources that the device uses. 16 (0x10)
        /// </summary>
        RessourcesIdentifyError = 0x10,
        /// <summary>
        /// Device is requesting an unknown resource type. 17 (0x11)
        /// </summary>
        RessourceRequestError = 0x11,
        /// <summary>
        /// Device drivers must be reinstalled. 18 (0x12)
        /// </summary>
        ReInstallDriverRequestedError = 0x12,
        /// <summary>
        /// Failure using the VxD loader. 19 (0x13)
        /// </summary>
        VxDLoaderError = 0x13,
        /// <summary>
        /// Registry might be corrupted. 20 (0x14)
        /// </summary>
        RegistryCorruptedError = 0x14,
        /// <summary>
        /// System failure. If changing the device driver is ineffective, see the hardware documentation. Windows is removing the device. 21 (0x15)
        /// </summary>
        SystemFailureDeviceRemovedError = 0x15,
        /// <summary>
        /// Device is disabled. 22 (0x16)
        /// </summary>
        DeviceDisabledError = 0x16,
        /// <summary>
        /// System failure. If changing the device driver is ineffective, see the hardware documentation. 23 (0x17)
        /// </summary>
        SystemFailureError = 0x17,
        /// <summary>
        /// Device is not present, not working properly, or does not have all of its drivers installed. 24 (0x18)
        /// </summary>
        DeviceNotPresentError = 0x18,
        /// <summary>
        /// Windows is still setting up the device. 25 (0x19)
        /// </summary>
        SetUpInProgressError = 0x19,
        /// <summary>
        /// Windows is still setting up the device. 26 (0x1A)
        /// </summary>
        SetUpInProgressError2 = 0x1A,
        /// <summary>
        /// Device does not have valid log configuration. 27 (0x1B)
        /// </summary>
        NoValidConfigurationError = 0x1B,
        /// <summary>
        /// Device drivers are not installed. 28 (0x1C)
        /// </summary>
        NoDriversInstalledError = 0x1C,
        /// <summary>
        /// Device is disabled. The device firmware did not provide the required resources. 29 (0x1D)
        /// </summary>
        FirmwareRessourceProvideError = 0x1D,
        /// <summary>
        /// Device is using an IRQ resource that another device is using. 30 (0x1E)
        /// </summary>
        IRGRessourceError = 0x1E,
        /// <summary>
        /// Device is not working properly. Windows cannot load the required device drivers. 31 (0x1F)
        /// </summary>
        DriverLoadError = 0x1F
    }

    /// <summary>
    /// dictionary for InterfaceType of Win32_DiskDrive
    /// </summary>
    public static class dictionaryInterfaceType
    {
        /// <summary>
        /// Small Computer System Interface / SCSI ("SCSI")
        /// </summary>
        public const string SCSI = "SCSI";
        /// <summary>
        /// Hard Disc Controller / HDC ("HDC")
        /// </summary>
        public const string HDC = "HDC";
        /// <summary>
        /// Integrated Device Electronics / IDE / ATA / PATA / SATA ("IDE")
        /// </summary>
        public const string IDE = "IDE";
        /// <summary>
        /// Universal Serial Bus / USB ("USB")
        /// </summary>
        public const string USB = "USB";
        /// <summary>
        /// IEEE 1394 / Firewire / i.Link ("1394")
        /// </summary>
        public const string FireWire = "1394";
    }

    /// <summary>
    /// dictionary for MediaType of Win32_DiskDrive
    /// </summary>
    public static class dictionaryMediaType
    {
        /// <summary>
        /// "External hars disk media"
        /// </summary>
        public const string External = "External hard disk media";
        /// <summary>
        /// "Removable media other than floppy"
        /// </summary>
        public const string Removable = "Removable media other than floppy";
        /// <summary>
        /// "Fixed hard disk media"
        /// </summary>
        public const string Fixed = "Fixed hard disk media";
        /// <summary>
        /// "Format is unknown"
        /// </summary>
        public const string Unknown = "Format is unknown";
    }

    /// <summary>
    /// enumeration for MediaType of Win32_Physicalmedia/CIM_PhysicalMedia
    /// </summary>
    public enum enumMediaType : ulong
    {
        /// <summary>
        /// Unknown (0)
        /// </summary>
        Unknown = 0,
        /// <summary>
        /// Other (1)
        /// </summary>
        Other = 1,
        /// <summary>
        /// Tape Cartridge (2)
        /// </summary>
        Tape = 2,
        /// <summary>
        /// QIC Cartridge (3)
        /// </summary>
        QIC = 3,
        /// <summary>
        /// AIT Cartridge (4)
        /// </summary>
        AIT = 4,
        /// <summary>
        /// DTF Cartridge (5)
        /// </summary>
        DTF = 5,
        /// <summary>
        /// DAT Cartridge (6)
        /// </summary>
        DAT = 6,
        /// <summary>
        /// 8mm Tape Cartridge (7)
        /// </summary>
        Tape_8 = 7,
        /// <summary>
        /// 19mm Tape Cartridge (8)
        /// </summary>
        Tape_19 = 8,
        /// <summary>
        /// DLT Cartridge (9)
        /// </summary>
        DLT = 9,
        /// <summary>
        /// Half-Inch Magnetic Tape Cartridge (10)
        /// </summary>
        Tape_HalfInch = 10,
        /// <summary>
        /// Cartridge Disk (11)
        /// </summary>
        Cartridge = 11,
        /// <summary>
        /// JAZ Disk (12)
        /// </summary>
        JAZ = 12,
        /// <summary>
        /// ZIP Disk (13)
        /// </summary>
        ZIP = 13,
        /// <summary>
        /// SyQuest Disk (14)
        /// </summary>
        SyQuest = 14,
        /// <summary>
        /// Winchester Removable Disk (15)
        /// </summary>
        Winchester = 15,
        /// <summary>
        /// CD-ROM (16)
        /// </summary>
        CD_ROM = 16,
        /// <summary>
        /// CD-ROM/XA (17)
        /// </summary>
        CD_ROM_XA = 17,
        /// <summary>
        /// CD-I (18)
        /// </summary>
        CD_I = 18,
        /// <summary>
        /// CD Recordable (19)
        /// </summary>
        CD_Recordable = 19,
        /// <summary>
        /// WORM (20)
        /// </summary>
        WORM = 20,
        /// <summary>
        /// Magneto-Optical (21)
        /// </summary>
        MO = 21,
        /// <summary>
        /// DVD (22)
        /// </summary>
        DVD = 22,
        /// <summary>
        /// DVD+RW (23)
        /// </summary>
        DVD_RW_p = 23,
        /// <summary>
        /// DVD-RAM (24)
        /// </summary>
        DVD_RAM = 24,
        /// <summary>
        /// DVD-ROM (25)
        /// </summary>
        DVD_ROM = 25,
        /// <summary>
        /// DVD-Video (26)
        /// </summary>
        DVD_Video = 26,
        /// <summary>
        /// Divx (27)
        /// </summary>
        Divx = 27,
        /// <summary>
        /// Floppy/Diskette (28)
        /// Floppy disk
        /// </summary>
        Floppy = 28,
        /// <summary>
        /// Floppy/Diskette (28)
        /// Floppy disk
        /// </summary>
        Diskette = 28,
        /// <summary>
        /// Hard Disk (29)
        /// </summary>
        HD = 29,
        /// <summary>
        /// Memory Card (30)
        /// </summary>
        MC = 30,
        /// <summary>
        /// Hard Copy (31)
        /// </summary>
        HC = 31,
        /// <summary>
        /// Clik Disk (32)
        /// </summary>
        Clik = 32,
        /// <summary>
        /// CD-RW (33)
        /// </summary>
        CD_RW_m = 33,
        /// <summary>
        /// CD-DA (34)
        /// </summary>
        CD_DA = 34,
        /// <summary>
        /// CD+ (35)
        /// </summary>
        CD_p = 35,
        /// <summary>
        /// DVD Recordable (36)
        /// </summary>
        DVD_Recordable = 36,
        /// <summary>
        /// DVD-RW (37)
        /// </summary>
        DVD_RW_m = 37,
        /// <summary>
        /// DVD-Audio (38)
        /// </summary>
        DVD_Audio = 38,
        /// <summary>
        /// DVD-5 (39)
        /// </summary>
        DVD_5 = 39,
        /// <summary>
        /// DVD-9 (40)
        /// </summary>
        DVD_9 = 40,
        /// <summary>
        /// DVD-10 (41)
        /// </summary>
        DVD_10 = 41,
        /// <summary>
        /// DVD-18 (42)
        /// </summary>
        DVD_18 = 42,
        /// <summary>
        /// Magneto-Optical Rewriteable (43)
        /// </summary>
        MO_RW = 43,
        /// <summary>
        /// Magneto-Optical Write Once (44)
        /// </summary>
        MO_WO = 44,
        /// <summary>
        /// Magneto-Optical Rewriteable (LIMDOW) (45)
        /// </summary>
        MO_RW_LIMDOW = 45,
        /// <summary>
        /// Phase Change Write Once (46)
        /// </summary>
        PC_WO = 46,
        /// <summary>
        /// Phase Change Rewriteable (47)
        /// </summary>
        PC_RW = 47,
        /// <summary>
        /// Phase Change Dual Rewriteable (48)
        /// </summary>
        PC_RW_dual = 48,
        /// <summary>
        /// Ablative Write Once (49)
        /// </summary>
        Ablative_WO = 49,
        /// <summary>
        /// Near Field Recording (50)
        /// </summary>
        NFR = 50,
        /// <summary>
        /// MiniQic (51)
        /// </summary>
        MiniQic = 51,
        /// <summary>
        /// Travan (52)
        /// </summary>
        Travan = 52,
        /// <summary>
        /// 8mm Metal Particle (53)
        /// </summary>
        MP_8 = 53,
        /// <summary>
        /// 8mm Advanced Metal Evaporate (54)
        /// </summary>
        AME_8 = 54,
        /// <summary>
        /// NCTP (55)
        /// </summary>
        NCTP = 55,
        /// <summary>
        /// LTO Ultrium (56)
        /// </summary>
        LTO_Ultrium = 56,
        /// <summary>
        /// LTO Accelis (57)
        /// </summary>
        LTO_Accelis = 57,
        /// <summary>
        /// 9 Track Tape (58)
        /// 9-track tape
        /// </summary>
        Tape_9Track = 58,
        /// <summary>
        /// 18 Track Tape (59)
        /// 18-track tape
        /// </summary>
        Tape_18Track = 59,
        /// <summary>
        /// 36 Track Tape (60)
        /// 36-track tape
        /// </summary>
        Tape_36Track = 60,
        /// <summary>
        /// Magstar 3590 (61)
        /// </summary>
        Magstar_3590 = 61,
        /// <summary>
        /// Magstar MP (62)
        /// </summary>
        Magstar_MP = 62,
        /// <summary>
        /// D2 Tape (63)
        /// </summary>
        Tape_D2 = 63,
        /// <summary>
        /// Tape - DST Small (64)
        /// Tape — DST small
        /// </summary>
        Tape_DST_Small = 64,
        /// <summary>
        /// Tape - DST Medium (65)
        /// Tape — DST medium
        /// </summary>
        Tape_DST_Medium = 65,
        /// <summary>
        /// Tape - DST Large (66)
        /// Tape — DST large
        /// </summary>
        Tape_DST_Large = 66
    }

    /// <summary>
    /// enumeration for FileSystemFlagsEx of Win32_CDROMDrive
    /// </summary>
    public enum enumFileSystemFlagsEx : uint
    {
        /// <summary>
        /// The file system supports case-sensitive file names.
        /// 1 (0x1)
        /// </summary>
        CASE_SENSITIVE_SEARCH = 0x1,
        /// <summary>
        /// The file system preserves the case of file names when it places a name on a disk.
        /// 2 (0x2)
        /// </summary>
        CASE_PRESERVED_NAMES = 0x2,
        /// <summary>
        /// The file system supports Unicode in file names as they appear on the disk.
        /// 4 (0x4)
        /// </summary>
        UNICODE_ON_DISK = 0x4,
        /// <summary>
        /// The file system preserves and enforces access control lists (ACLs). For example, the NTFS file system preserves and enforces ACLs and the FAT file system does not.
        /// 8 (0x8)
        /// </summary>
        PERSISTENT_ACLS = 0x8,
        /// <summary>
        /// The file system supports file-based compression.
        /// 16 (0x10)
        /// </summary>
        FILE_COMPRESSION = 0x10,
        /// <summary>
        /// The file system supports disk quotas.
        /// 32 (0x20)
        /// </summary>
        VOLUME_QUOTAS = 0x20,
        /// <summary>
        /// The file system supports spare files.
        /// 64 (0x40)
        /// </summary>
        SUPPORTS_SPARSE_FILES = 0x40,
        /// <summary>
        /// The file system supports reparse points.
        /// 128 (0x80)
        /// </summary>
        SUPPORTS_REPARSE_POINTS = 0x80,
        /// <summary>
        /// The file system supports the remote storage of files.
        /// 256 (0x100)
        /// </summary>
        SUPPORTS_REMOTE_STORAGE = 0x100,
        /// <summary>
        /// The file system supports file names that are longer than eight characters.
        /// 16384 (0x4000)
        /// </summary>
        SUPPORTS_LONG_NAMES = 0x4000,
        /// <summary>
        /// The specified disk volume is a compressed volume, for example, a DoubleSpace volume.
        /// 32768 (0x8000)
        /// </summary>
        VOLUME_IS_COMPRESSED = 0x8000,
        /// <summary>
        /// The specified volume is read-only.
        /// 524289 (0x80001)
        /// </summary>
        READ_ONLY_VOLUME = 0x80001,
        /// <summary>
        /// The file system supports object identifiers.
        /// 65536 (0x10000)
        /// </summary>
        SUPPORTS_OBJECT_IDS = 0x10000,
        /// <summary>
        /// The file system supports the Encrypted File System (EFS).
        /// 131072 (0x20000)
        /// </summary>
        SUPPORTS_ENCRYPTION = 0x20000,
        /// <summary>
        /// The file system supports named streams.
        /// 262144 (0x40000)
        /// </summary>
        SUPPORTS_NAMED_STREAMS = 0x40000
    }


    /// <summary>
    /// enumeration for OperationalStatus of CIM_LogicalDevice
    /// </summary>
    public enum enumOperationalStatus : ushort
    {
        /// <summary>
        /// An unknown error has occurred.
        /// </summary>
        Unknown = 0,
        /// <summary>
        /// An error other than one on this list has occurred.
        /// </summary>
        Other = 1,
        /// <summary>
        /// The element is OK.
        /// </summary>
        OK = 2,
        /// <summary>
        /// The element has been degraded.
        /// </summary>
        Degraded = 3,
        /// <summary>
        /// The element is functioning, but needs attention. Examples of Stressed states are overload, overheated, and so on.
        /// </summary>
        Stressed = 4,
        /// <summary>
        /// An element is functioning nominally but predicting a failure in the near future.
        /// </summary>
        PredictiveFailure = 5,
        /// <summary>
        /// An error has occurred.
        /// </summary>
        Error = 6,
        /// <summary>
        /// A non-recoverable error has occurred.
        /// </summary>
        NonRecoverableError = 7,
        /// <summary>
        /// The element is in the process of starting.
        /// </summary>
        Starting = 8,
        /// <summary>
        /// The element is in the process of stopping.
        /// </summary>
        Stopping = 9,
        /// <summary>
        /// A clean and orderly stop has occurred.
        /// </summary>
        Stopped = 10,
        /// <summary>
        /// An element being configured, maintained, cleaned, or otherwise administered.
        /// </summary>
        InService = 11,
        /// <summary>
        /// The monitoring system has knowledge of this element, but has never been able to establish communications with it.
        /// </summary>
        NoContact = 12,
        /// <summary>
        /// The Managed System Element is known to exist and has been contacted successfully in the past, but is currently unreachable.
        /// </summary>
        LostCommunication = 13,
        /// <summary>
        /// An abrupt stop has occurred; the state and configuration of the element might need to be updated.
        /// </summary>
        Aborted = 14,
        /// <summary>
        /// The element is inactive or quiesced.
        /// </summary>
        Dormant = 15,
        /// <summary>
        /// The current element might be OK, but that another element, on which it is dependent, is in error. An example is a network service or endpoint that cannot function due to lower-layer networking problems.
        /// </summary>
        SupportingEntityInError = 16,
        /// <summary>
        /// the element has completed its operation. This value should be combined with either OK, Error, or Degraded so that a client can tell if the complete operation Completed with OK (passed), Completed with Error (failed), or Completed with Degraded (the operation finished, but it did not complete OK or did not report an error)
        /// </summary>
        Completed = 17,
        /// <summary>
        /// The element has additional power model information contained in the Associated PowerManagementService association.
        /// </summary>
        PowerMode = 18,
        /// <summary>
        /// indicates the element is being relocated. OperationalStatus replaces the Status property on ManagedSystemElement to provide a consistent approach to enumerations, to address implementation needs for an array property, and to provide a migration path from today\'s environment to the future. This change was not made earlier because it required the deprecated qualifier. Due to the widespread use of the existing Status property in management applications, it is strongly recommended that providers or instrumentation provide both the Status and OperationalStatus properties. Further, the first value of OperationalStatus should contain the primary status for the element. When instrumented, Status (because it is single-valued) should also provide the primary status of the element.
        /// </summary>
        Relocating = 19,
        /// <summary>
        /// DMTF has reserved this portion of the range for additional OperationalStatus values in the future.
        /// 19-32767
        /// </summary>
        DMTFReserved_first = 20,
        /// <summary>
        /// DMTF has reserved this portion of the range for additional OperationalStatus values in the future.
        /// 19-32767
        /// </summary>
        DMTFReserved_last = 32767,
        /// <summary>
        /// Microsoft has reserved the unused portion of the range for additional OperationalStatus values in the future.
        /// 32768-65535
        /// </summary>
        VendorReserved_first = 32768,
        /// <summary>
        /// Microsoft has reserved the unused portion of the range for additional OperationalStatus values in the future.
        /// 32768-65535
        /// </summary>
        VendorReserved_last = 65535
    }

    /// <summary>
    /// enumeration for PowerManagementCapabilities of CIM_LogicalDevice
    /// </summary>
    public enum enumPowerManagementCapabilities : ushort
    {
        /// <summary>
        /// Unknown
        /// </summary>
        Unknown = 0,
        /// <summary>
        /// Not Supported
        /// </summary>
        NotSupported = 1,
        /// <summary>
        /// Disabled
        /// </summary>
        Disabled = 2,
        /// <summary>
        /// The power management features are currently enabled but the exact feature set is unknown or the information is unavailable.
        /// </summary>
        Enabled = 3,
        /// <summary>
        /// The device can change its power state based on usage or other criteria.
        /// </summary>
        PowerSavingModesEnteredAutomatically = 4,
        /// <summary>
        /// The SetPowerState method is supported. This method is found on the parent CIM_LogicalDevice class and can be implemented. For more information, see Designing Managed Object Format (MOF) Classes.
        /// </summary>
        PowerStateSettable = 5,
        /// <summary>
        /// The SetPowerState method can be invoked with the PowerState parameter set to 5 ("Power Cycle").
        /// </summary>
        PowerCyclingSupported = 6,
        /// <summary>
        /// The SetPowerState method can be invoked with the PowerStateparameter set to 5 ("Power Cycle") and Time set to a specific date and time, or interval, for power-on.
        /// </summary>
        TimedPowerOnSupported = 7
    }

    /// <summary>
    /// enumeation for PowerState in Method SetPowerState of CIM_LogicalDevice
    /// </summary>
    public enum enumPowerStateValueMap : ushort
    {
        /// <summary>
        /// Full power.
        /// </summary>
        FullPower = 1,
        /// <summary>
        /// Power save — low-power mode.
        /// </summary>
        PoweSave_LowPowerMode = 2,
        /// <summary>
        /// Power save — standby.
        /// </summary>
        PowerSave_Standby = 3,
        /// <summary>
        /// Power save — other.
        /// </summary>
        PowerSave_Other = 4,
        /// <summary>
        /// Power cycle.
        /// </summary>
        PowerCycle = 5,
        /// <summary>
        /// Power off.
        /// </summary>
        PowerOff = 6
    }
}