﻿namespace libPlugNPlay
{


    ///// <summary>
    ///// Additional availability and status of the Device, beyond that specified in the Availability property. The Availability property denotes the primary status and availability of the Device. In some cases, this will not be sufficient to denote the complete status of the Device. In those cases, the AdditionalAvailability property can be used to provide further information. For example, a Device's primary Availability may be Off line (value=8), but it may also be in a low power state (AdditonalAvailability value=14), or the Device could be running Diagnostics (AdditionalAvailability value=5, In Test)."
    ///// </summary>
    ///// <remarks>Write-only</remarks>
    ///// <see cref="enumAvailability"/>
    //[libPlugNPlay.WMIProperty]
    //public libPlugNPlay.enumAvailability AdditionalAvailability { private get; set; }

    ///// <summary>
    ///// An array of free-form strings providing explanations and details behind the entries in the OtherIdentifyingInfo array. Note, each entry of this array is related to the entry in OtherIdentifyingInfo that is located at the same index.
    ///// </summary>
    ///// <remarks>Length = 1</remarks>
    //[libPlugNPlay.WMIProperty]
    //public string[] IdentifyingDescriptions { get; protected set; }


    ///// <summary>
    ///// Maximum time in milliseconds, that a Device can run in a Quiesced state. A Device's state is defined in its Availability and AdditionalAvailability properties, where Quiesced is conveyed by the value 21. What occurs at the end of the time limit is device-specific. The Device may unquiesce, may offline or take other action. A value of 0 indicates that a Device can remain quiesced indefinitely.
    ///// <para />Note:
    ///// <para />The MaxQuiesceTime property has been deprecated. When evaluating the use of Quiesce, it was determine that this single property is not adequate for describing when a device will automatically exit a quiescent state. In fact, the most likely scenario for a device to exit a quiescent state was determined to be based on the number of outstanding requests queued rather than on a maximum time. This will be re-evaluated and repositioned later.
    ///// </summary>
    //[libPlugNPlay.WMIProperty]
    //public System.UInt64 MaxQuiesceTime { get; protected set; }

    ///// <summary>
    ///// The current statuses of the element. This property is inherited from CIM_ManagedSystemElement.
    ///// </summary>
    //[libPlugNPlay.WMIProperty]
    //public libPlugNPlay.enumOperationalStatus OperationalStatus { get; protected set; }

    ///// <summary>
    ///// Array that captures additional data, beyond DeviceID information, that could be used to identify a LogicalDevice. One example would be to hold the Operating System's user friendly name for the Device in this property. Maximum length is 256.
    ///// </summary>
    //[libPlugNPlay.WMIProperty]
    //public System.UInt64 OtherIdentifyingInfo { get; protected set; }

    ///// <summary>
    ///// The number of consecutive hours that this Device has been powered, since its last power cycle.
    ///// </summary>
    //[libPlugNPlay.WMIProperty]
    //public System.UInt64 PowerOnHours { get; protected set; }

    ///// <summary>
    ///// The total number of hours that this device has been powered.
    ///// </summary>
    //[libPlugNPlay.WMIProperty]
    //public System.UInt64 TotalPowerOnHours { get; protected set; }


    #region Top
    #region Mass Storage Classes
    /// <summary>
    /// The CIM_CDROMDrive class represents a CD-ROM drive on the computer.
    /// <para />Note  The name of the drive does not correspond to the logical drive letter assigned to the device, which is the name of the logical storage device that is dependent on the drive.
    /// <para />Important  The DMTF (Distributed Management Task Force) CIM (Common Information Model) classes are the parent classes upon which WMI classes are built. WMI currently supports only the CIM 2.x version schemas. 
    /// </summary>
    /// <remarks>The CIM_CDROMDrive class is derived from CIM_MediaAccessDevice.
    /// <para />WMI does not implement this class. For more information about classes derived from CIM_CDROMDrive, see Win32 Classes.
    /// <para />This documentation is derived from the CIM class descriptions published by the DMTF. Microsoft may have made changes to correct minor errors, conform to Microsoft SDK documentation standards, or provide more information.</remarks>
    /// <remarks>Namespace: Root\CIMV2</remarks>
    /// <remarks>MOF: CIMWin32.mof</remarks>
    /// <remarks>DLL: CIMWin32.dll</remarks>
    public abstract class CIM_CDROMDrive : CIM_MediaAccessDevice
    {
        public new const string UUID = "{8502C52B-5FBB-11D2-AAC1-006008C78BC7}";
        public new const string WMI_Namespace = @"Root\CIMV2";
        public new const string WMI_ClassName = "CIM_CDROMDrive";

        #region Methods
        #region inherits from CIM_MediaAccessDevice
        #region inherited from CIM_LogicalDevice
        //System.UInt32 Reset();
        //System.UInt32 SetPowerState(libPlugNPlay.enumPowerStateValueMap PowerState, System.DateTime Time);
        #endregion
        #endregion
        #endregion

        #region Properties
        #region inherited from CIM_MediaAccessDevice
        //System.UInt16[] Capabilities;
        //string[] CapabilityDescriptions;
        //string CompressionMethod;
        //System.UInt64? DefaultBlockSize;
        //string ErrorMethodology;
        //System.UInt64? MaxBlockSize;
        //System.UInt64? MaxMediaSize;
        //System.UInt64? MinBlockSize;
        //bool? NeedsCleaning;
        //System.UInt32? NumberOfMediaSupported;
        #region inherited from CIM_LogicalDevice
        //libPlugNPlay.enumAvailability? Availability;
        //libPlugNPlay.enumWConfigManagerErrorCode? ConfigManagerErrorCode;
        //bool? ConfigManagerUserConfig;
        //string CreationClassName;
        //string DeviceID;
        //bool? ErrorCleared;
        //string ErrorDescription;
        //System.UInt32? LastErrorCode;
        //string PNPDeviceID;
        //System.UInt16[] PowerManagementCapabilities;
        //bool? PowerManagementSupported;
        //System.UInt16? StatusInfo;
        //string SystemCreationClassName;
        //string SystemName;
        #region inherited from CIM_LogicalElement
        #region inherited from CIM_ManagedSystemElement
        //string Caption;
        //string Description;
        //System.DateTime InstallDate;
        //string Name;
        //string Status;
        #endregion
        #endregion
        #endregion
        #endregion
        #endregion
    }

    /// <summary>
    /// The CIM_DiskDrive class represents a physical disk drive as seen by the operating system. The disk drive features correspond to the logical and management characteristics of the drive, and in some cases, may not reflect the physical characteristics of the device. An interface to a physical drive is a member of this class. However, an object based on another logical device is not a member of this class.
    /// <para />Important  The DMTF (Distributed Management Task Force) CIM (Common Information Model) classes are the parent classes upon which WMI classes are built. WMI currently supports only the CIM 2.x version schemas. 
    /// </summary>
    /// <remarks>The CIM_DiskDrive class is derived from CIM_MediaAccessDevice.
    /// <para />WMI does not implement this class. See Win32 Classes for classes derived from CIM_DiskDrive.
    /// <para />This documentation is derived from the CIM class descriptions published by the DMTF. Microsoft may have made changes to correct minor errors, conform to Microsoft SDK documentation standards, or provide more information.</remarks>
    /// <remarks>Namespace: Root\CIMV2</remarks>
    /// <remarks>MOF: CIMWin32.mof</remarks>
    /// <remarks>DLL: CIMWin32.dll</remarks>
    abstract class CIM_DiskDrive : CIM_MediaAccessDevice
    {
        public new const string UUID = "{8502C52C-5FBB-11D2-AAC1-006008C78BC7}";
        public new const string WMI_Namespace = @"Root\CIMV2";
        public new const string WMI_ClassName = "CIM_DiskDrive";

        #region Methods
        #region inherits from CIM_MediaAccessDevice
        #region inherited from CIM_LogicalDevice
        //System.UInt32 Reset();
        //System.UInt32 SetPowerState(libPlugNPlay.enumPowerStateValueMap PowerState, System.DateTime Time);
        #endregion
        #endregion
        #endregion

        #region Properties
        #region inherited from CIM_MediaAccessDevice
        //System.UInt16[] Capabilities;
        //string[] CapabilityDescriptions;
        //string CompressionMethod;
        //System.UInt64? DefaultBlockSize;
        //string ErrorMethodology;
        //System.UInt64? MaxBlockSize;
        //System.UInt64? MaxMediaSize;
        //System.UInt64? MinBlockSize;
        //bool? NeedsCleaning;
        //System.UInt32? NumberOfMediaSupported;
        #region inherited from CIM_LogicalDevice
        //libPlugNPlay.enumAvailability? Availability;
        //libPlugNPlay.enumWConfigManagerErrorCode? ConfigManagerErrorCode;
        //bool? ConfigManagerUserConfig;
        //string CreationClassName;
        //string DeviceID;
        //bool? ErrorCleared;
        //string ErrorDescription;
        //System.UInt32? LastErrorCode;
        //string PNPDeviceID;
        //System.UInt16[] PowerManagementCapabilities;
        //bool? PowerManagementSupported;
        //System.UInt16? StatusInfo;
        //string SystemCreationClassName;
        //string SystemName;
        #region inherited from CIM_LogicalElement
        #region inherited from CIM_ManagedSystemElement
        //string Caption;
        //string Description;
        //System.DateTime InstallDate;
        //string Name;
        //string Status;
        #endregion
        #endregion
        #endregion
        #endregion
        #endregion
    }

    /// <summary>
    /// The CIM_DisketteDrive class represents the capabilities and management of a diskette drive.
    /// <para />Important  The DMTF (Distributed Management Task Force) CIM (Common Information Model) classes are the parent classes upon which WMI classes are built. WMI currently supports only the CIM 2.x version schemas.
    /// </summary>
    /// <remarks>The CIM_DisketteDrive class is derived from CIM_MediaAccessDevice.
    /// <para />WMI does not implement this class. For classes derived from CIM_DisketteDrive, see Win32 Classes.</remarks>
    /// <remarks>Namespace: Root\CIMV2</remarks>
    /// <remarks>MOF: CIMWin32.mof</remarks>
    /// <remarks>DLL: CIMWin32.dll</remarks>
    public abstract class CIM_DisketteDrive : CIM_MediaAccessDevice
    {
        public new const string UUID = "{F27851CD-BBAC-11d2-85E5-0000F8102E5F}";
        public new const string WMI_Namespace = @"Root\CIMV2";
        public new const string WMI_ClassName = "CIM_DisketteDrive";

        #region Methods
        #region inherits from CIM_MediaAccessDevice
        #region inherited from CIM_LogicalDevice
        //System.UInt32 Reset();
        //System.UInt32 SetPowerState(libPlugNPlay.enumPowerStateValueMap PowerState, System.DateTime Time);
        #endregion
        #endregion
        #endregion

        #region Properties
        #region inherited from CIM_MediaAccessDevice
        //System.UInt16[] Capabilities;
        //string[] CapabilityDescriptions;
        //string CompressionMethod;
        //System.UInt64? DefaultBlockSize;
        //string ErrorMethodology;
        //System.UInt64? MaxBlockSize;
        //System.UInt64? MaxMediaSize;
        //System.UInt64? MinBlockSize;
        //bool? NeedsCleaning;
        //System.UInt32? NumberOfMediaSupported;
        #region inherited from CIM_LogicalDevice
        //libPlugNPlay.enumAvailability? Availability;
        //libPlugNPlay.enumWConfigManagerErrorCode? ConfigManagerErrorCode;
        //bool? ConfigManagerUserConfig;
        //string CreationClassName;
        //string DeviceID;
        //bool? ErrorCleared;
        //string ErrorDescription;
        //System.UInt32? LastErrorCode;
        //string PNPDeviceID;
        //System.UInt16[] PowerManagementCapabilities;
        //bool? PowerManagementSupported;
        //System.UInt16? StatusInfo;
        //string SystemCreationClassName;
        //string SystemName;
        #region inherited from CIM_LogicalElement
        #region inherited from CIM_ManagedSystemElement
        //string Caption;
        //string Description;
        //System.DateTime InstallDate;
        //string Name;
        //string Status;
        #endregion
        #endregion
        #endregion
        #endregion
        #endregion
    }

    /// <summary>
    /// The CIM_PhysicalMedia class represents types of documentation and storage medium, such as tapes, CD ROMs, and so on. This class is typically used to locate and manage removable media (versus media sealed with the media access device as a single package, such as hard disks). Sealed media, however, can also be modeled using this class when the media is associated with the physical package using the CIM_PackagedComponent relationship.
    /// <para />Important  The DMTF (Distributed Management Task Force) CIM (Common Information Model) classes are the parent classes upon which WMI classes are built. WMI currently supports only the CIM 2.x version schemas. 
    /// </summary>
    /// <remarks>The CIM_PhysicalMedia class is derived from CIM_PhysicalComponent.
    /// <para />WMI does not implement this class.</remarks>
    /// <remarks>Namespace: Root\CIMV2</remarks>
    /// <remarks>MOF: CIMWin32.mof</remarks>
    /// <remarks>DLL: CIMWin32.dll</remarks>
    public abstract class CIM_PhysicalMedia : CIM_PhysicalComponent
    {
        public new const string UUID = "{FAF76B7D-798C-11D2-AAD1-006008C78BC7}";
        public new const string WMI_Namespace = @"Root\CIMV2";
        public new const string WMI_ClassName = "CIM_LogicalDevice";

        #region Properties
        #region inherited from CIM_PhysicalComponent
        #region inherited from CIM_PhysicalElement
        #region inherited from CIM_ManagedSystemelement
        //string Caption;
        //string Description;
        //System.DateTime InstallDate;
        //string Name;
        //string Status;
        #endregion
        //string CreationClassName;
        //string Manufacturer;
        //string Model;
        //string OtherIdentifyingInfo;
        //string PartNumber;
        //bool? PoweredOn;
        //string SerialNumber;
        //string SKU;
        //string Tag;
        //string Version;
        #endregion
        //bool? HotSwappable;
        //bool? Removable;
        //bool? Replaceable;
        #endregion
        /// <summary>
        /// Number of bytes that can be read from, or written to, a media. This property is not applicable to hard copy (documentation) or cleaner media. Data compression should not be assumed, as it would increase the value in this property. For tapes, it should be assumed that no file marks or blank space areas are recorded on the media.
        /// <para />For more information about using uint64 values in scripts, see Scripting in WMI.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public System.UInt64? Capacity { get; private set; }
        /// <summary>
        /// If TRUE, the physical media is used for cleaning purposes, not data storage.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public bool? CleanerMedia { get; private set; }
        /// <summary>
        /// Additional detail related to the MediaType enumeration. For example, if value 3 ("QIC Cartridge") is specified, this property would indicate whether the tape is wide or 1/4 inch, pre-formatted, Travan compatible, and so on.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public string MediaDescription { get; private set; }
        /// <summary>
        /// Type of physical media. The MediaDescription property provides more explicit definition of the media type.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public enumMediaType? MediaType { get; private set; }
        /// <summary>
        /// If TRUE, the media is currently write-protected by a physical mechanism, such as a protect tab on a floppy disk.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public bool? WriteProtectOn { get; private set; }
        #endregion
    }

    /// <summary>
    /// The CIM_TapeDrive class represents a tape drive on the system. Tape drives are primarily distinguished in that they can only be accessed sequentially.
    /// <para />Important  The DMTF (Distributed Management Task Force) CIM (Common Information Model) classes are the parent classes upon which WMI classes are built. WMI currently supports only the CIM 2.x version schemas.
    /// </summary>
    /// <remarks>The CIM_TapeDrive class is derived from CIM_MediaAccessDevice.
    /// <para />WMI does not implement this class. For WMI classes derived from CIM_TapeDrive, see Win32 Classes.</remarks>
    /// <remarks>Namespace: Root\CIMV2</remarks>
    /// <remarks>MOF: CIMWin32.mof</remarks>
    /// <remarks>DLL: CIMWin32.dll</remarks>
    public abstract class CIM_TapeDrive : CIM_MediaAccessDevice
    {
        public new const string UUID = "{8502C52D-5FBB-11D2-AAC1-006008C78BC7}";
        public new const string WMI_Namespace = @"Root\CIMV2";
        public new const string WMI_ClassName = "CIM_TapeDrive";

        #region Methods
        #region inherits from CIM_MediaAccessDevice
        #region inherited from CIM_LogicalDevice
        //System.UInt32 Reset();
        //System.UInt32 SetPowerState(libPlugNPlay.enumPowerStateValueMap PowerState, System.DateTime Time);
        #endregion
        #endregion
        #endregion

        #region Properties
        #region inherited from CIM_MediaAccessDevice
        //System.UInt16[] Capabilities;
        //string[] CapabilityDescriptions;
        //string CompressionMethod;
        //System.UInt64? DefaultBlockSize;
        //string ErrorMethodology;
        //System.UInt64? MaxBlockSize;
        //System.UInt64? MaxMediaSize;
        //System.UInt64? MinBlockSize;
        //bool? NeedsCleaning;
        //System.UInt32? NumberOfMediaSupported;
        #region inherited from CIM_LogicalDevice
        //libPlugNPlay.enumAvailability? Availability;
        //libPlugNPlay.enumWConfigManagerErrorCode? ConfigManagerErrorCode;
        //bool? ConfigManagerUserConfig;
        //string CreationClassName;
        //string DeviceID;
        //bool? ErrorCleared;
        //string ErrorDescription;
        //System.UInt32? LastErrorCode;
        //string PNPDeviceID;
        //System.UInt16[] PowerManagementCapabilities;
        //bool? PowerManagementSupported;
        //System.UInt16? StatusInfo;
        //string SystemCreationClassName;
        //string SystemName;
        #region inherited from CIM_LogicalElement
        #region inherited from CIM_ManagedSystemElement
        //string Caption;
        //string Description;
        //System.DateTime InstallDate;
        //string Name;
        //string Status;
        #endregion
        #endregion
        #endregion
        #endregion
        /// <summary>
        /// Size, in bytes, of the area designated as end-of-tape. Access in this area generates an end-of-tape warning.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public System.UInt32? EOTWarningZoneSize { get; private set; }
        /// <summary>
        /// Maximum partition count for the tape drive.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public System.UInt32? MaxPartitionCount { get; private set; }
        /// <summary>
        /// Number of bytes inserted between blocks on a tape media.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public System.UInt32? Padding { get; private set; }
        #endregion
    }
    #endregion
    #endregion

    #region 2nd
    /// <summary>
    /// The CIM_MediaAccessDevice class represents the ability to access one or more media, and then use the media to store and retrieve data.
    /// <para />Important  The DMTF (Distributed Management Task Force) CIM (Common Information Model) classes are the parent classes upon which WMI classes are built. WMI currently supports only the CIM 2.x version schemas.
    /// </summary>
    /// <remarks>The CIM_MediaAccessDevice class is derived from CIM_LogicalDevice.
    /// <para />WMI does not implement this class. For classes derived from CIM_MediaAccessDevice, see Win32 Classes.
    /// <para />This documentation is derived from the CIM class descriptions published by the DMTF. Microsoft may have made changes to correct minor errors, conform to Microsoft SDK documentation standards, or provide more information.</remarks>
    /// <remarks>Namespace: Root\CIMV2</remarks>
    /// <remarks>MOF: CIMWin32.mof</remarks>
    /// <remarks>DLL: CIMWin32.dll</remarks>
    public abstract class CIM_MediaAccessDevice : CIM_LogicalDevice
    {
        public new const string UUID = "{8502C52A-5FBB-11D2-AAC1-006008C78BC7}";
        public new const string WMI_Namespace = @"Root\CIMV2";
        public new const string WMI_ClassName = "CIM_MediaAccessDevice";

        #region Methods
        #region inherited from CIM_LogicalDevice
        //System.UInt32 Reset();
        //System.UInt32 SetPowerState(libPlugNPlay.enumPowerStateValueMap PowerState, System.DateTime Time);
        #endregion
        #endregion

        #region Properties
        #region inherited from CIM_LogicalDevice
        //libPlugNPlay.enumAvailability? Availability;
        //bool? ConfigManagerUserConfig;
        //string CreationClassName;
        //string DeviceID;
        //bool? ErrorCleared;
        //string ErrorDescription;
        //System.UInt32? LastErrorCode;
        //string PNPDeviceID;
        //System.UInt16[] PowerManagementCapabilities;
        //bool? PowerManagementSupported;
        //System.UInt16? StatusInfo;
        //string SystemCreationClassName;
        //string SystemName;
        #region inherited from CIM_LogicalElement
        #region inherited from CIM_ManagedSystemElement
        //string Caption;
        //string Description;
        //System.DateTime InstallDate;
        //string Name;
        //string Status;
        #endregion
        #endregion
        #endregion
        /// <summary>
        /// Capabilities of the media access device. 
        /// </summary>
        /// <remarks>ArrayType ("Indexed")</remarks>
        /// <remarks>MappingStrings ("MIF.DMTF|Storage Devices|001.9", "MIF.DMTF|Storage Devices|001.11", "MIF.DMTF|Storage Devices|001.12", "MIF.DMTF|Disks|003.7")</remarks>
        /// <remarks>ModelCorrespondence ("CIM_MediaAccessDevice.CapabilityDescriptions")</remarks>
        [libPlugNPlay.WMIProperty]
        public libPlugNPlay.emunCapabilities[] Capabilities { get; protected set; }
        /// <summary>
        /// Array of free-form strings that provides detailed explanations for access device features indicated in the Capabilities array.
        /// <para />Note   Each entry of this array is related to the entry in the Capabilities array, located at the same index.
        /// </summary>
        /// <remarks>ArrayType ("Indexed")</remarks>
        /// <remarks>ModelCorrespondence ("CIM_MediaAccessDevice.Capabilities")</remarks>
        [libPlugNPlay.WMIProperty]
        public string[] CapabilityDescriptions { get; protected set; }
        /// <summary>
        /// Free-form string that indicates the algorithm or tool used to compress the logical file. If the compression scheme is unknown or not described, use "Unknown". If the logical file is compressed, but the compression scheme is unknown or not described, use "Compressed". If the logical file is not compressed, use "Not Compressed". 
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public string CompressionMethod { get; protected set; }
        /// <summary>
        /// Windows Configuration Manager error code.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public libPlugNPlay.enumConfigManagerErrorCode? ConfigManagerErrorCode { get; protected set; }
        /// <summary>
        /// Default block size, in bytes, for the device.
        /// <para />For more information about using System.UInt64 values in scripts, see Scripting in WMI.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public System.UInt64? DefaultBlockSize { get; protected set; }
        /// <summary>
        /// Free-form string that describes the types of error detection and correction supported by the device.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public string ErrorMethodology { get; protected set; }
        /// <summary>
        /// Maximum block size, in bytes, for media accessed by the device.
        /// <para />For more information about using System.UInt64 values in scripts, see Scripting in WMI.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public System.UInt64? MaxBlockSize { get; protected set; }
        /// <summary>
        /// Maximum size, in kilobytes, of media supported by this device. Kilobytes are interpreted as the number of bytes multiplied by 1000 (not the number of bytes multiplied by 1024).
        /// <para />For more information about using System.UInt64 values in scripts, see Scripting in WMI.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public System.UInt64? MaxMediaSize { get; protected set; }
        /// <summary>
        /// Minimum block size, in bytes, for media accessed by the device.
        /// <para />For more information about using System.UInt64 values in scripts, see Scripting in WMI.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public System.UInt64? MinBlockSize { get; protected set; }
        /// <summary>
        /// If TRUE, the media access device needs cleaning. Whether manual or automatic cleaning is possible is indicated in the Capabilities array property.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public bool? NeedsCleaning { get; protected set; }
        /// <summary>
        /// Maximum number of multiple individual media that can be supported or inserted.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public System.UInt32? NumberOfMediaSupported { get; protected set; }
        #endregion
    }
    #endregion

    #region intermediate
    #region logical
    /// <summary>
    ///  The CIM_LogicalDevice class represents a hardware entity that may or may not be realized in physical hardware.
    ///  <para />Important  The DMTF (Distributed Management Task Force) CIM (Common Information Model) classes are the parent classes upon which WMI classes are built. WMI currently supports only the CIM 2.x version schemas. 
    /// </summary>
    /// <remarks>Logical device characteristics that manage operation or configuration are contained in, or associated with, the CIM_LogicalDevice object. Printer operational properties, for example, are supported paper sizes or detected errors. Sensor device configuration properties, for example, are threshold settings. Various configurations can exist for a logical device and are contained in the CIM_Setting objects, which are associated with the logical device.
    /// <para />The CIM_LogicalDevice class is derived from CIM_LogicalElement.
    /// <para />WMI does not implement this class. For classes derived from CIM_LogicalDevice, see Win32 Classes.
    /// <para />This documentation is derived from the CIM class descriptions published by the DMTF. Microsoft may have made changes to correct minor errors, conform to Microsoft SDK documentation standards, or provide more information.</remarks>
    /// <remarks>Namespace: Root\CIMV2</remarks>
    /// <remarks>MOF: CIMWin32.mof</remarks>
    /// <remarks>DLL: CIMWin32.dll</remarks>
    public abstract class CIM_LogicalDevice : CIM_LogicalElement
    {
        public new const string UUID = "{8502C529-5FBB-11D2-AAC1-006008C78BC7}";
        public new const string WMI_Namespace = @"Root\CIMV2";
        public new const string WMI_ClassName = "CIM_LogicalDevice";

        #region Methods
        /// <summary>
        /// The Reset method of the CIM_LogicalDevice class requests a reset of the logical device. This method is inherited from CIM_LogicalDevice.
        /// </summary>
        /// <returns>Returns 0 (zero) if the request was successfully executed, 1 (one) if the request is not supported, and some other value if an error occurred.</returns>
        /// <remarks>This method is currently not implemented by WMI. To use this method, you must implement it in your own provider.
        /// <para />This documentation is derived from the CIM class descriptions published by the DMTF. Microsoft may have made changes to correct minor errors, conform to Microsoft SDK documentation standards, or provide more information.</remarks>
        [libPlugNPlay.WMIMethod]
        public System.UInt32 Reset()
        {
            throw new System.NotImplementedException("This method is currently not implemented by WMI. To use this method, you must implement it in your own provider.");
        }
        /// <summary>
        /// The SetPowerState method of the CIM_LogicalDevice class sets the desired power state for a logical device and when a device should be put into that state.
        /// </summary>
        /// <param name="PowerState">A ValueMap value that specifies the desired power state for this logical device.</param>
        /// <param name="Time">Specifies when the power state should be set, either as a regular date-time value or as an interval value (where the interval begins when the method invocation is received). When the PowerState parameter is equal to 5 ("Power Cycle"), the Time parameter indicates when the device should power on again. Power-off is immediate.</param>
        /// <returns>Returns 0 (zero) if successful, 1 (one) if the specified PowerState and Time request is not supported, and another value if any other error occurred.</returns>
        /// <remarks> In a subclass, the set of possible return codes should be specified by using a ValueMap qualifier on the method. The strings to which the ValueMap contents are translated should also be specified in the subclass as a Values array qualifier. This method is inherited from CIM_LogicalDevice.
        /// <para />This method is currently not implemented by WMI. To use this method, you must implement it in your own provider.
        /// <para />This documentation is derived from the CIM class descriptions published by the DMTF. Microsoft may have made changes to correct minor errors, conform to Microsoft SDK documentation standards, or provide more information.</remarks>
        [libPlugNPlay.WMIMethod]
        public System.UInt32 SetPowerState(libPlugNPlay.enumPowerStateValueMap PowerState, System.DateTime Time)
        {
            throw new System.NotImplementedException("This method is currently not implemented by WMI. To use this method, you must implement it in your own provider.");
        }
        #endregion

        #region Properties
        #region inherited from CIM_LogicalElement
        #region inherited from CIM_ManagedsystemElement
        //string Caption;
        //string Description;
        //System.DateTime InstallDate;
        //string Name;
        //string Status;
        #endregion
        #endregion

        /// <summary>
        /// Availability and status of the device.
        /// </summary>
        /// <remarks>MappingStrings ("MIF.DMTF|Operational State|003.5", "MIB.IETF|HOST-RESOURCES-MIB.hrDeviceStatus")</remarks>
        /// <see cref="enumAvailability"/>
        [libPlugNPlay.WMIProperty]
        public libPlugNPlay.enumAvailability? Availability { get; protected set; }
        /// <summary>
        /// Win32 Configuration Manager error code.
        /// </summary>
        /// <remarks>Schema ("Win32")</remarks>
        [libPlugNPlay.WMIProperty]
        public libPlugNPlay.enumConfigManagerErrorCode? ConfigManagerErrorCode { get; protected set; }
        /// <summary>
        /// If TRUE, the device is using a user-defined configuration.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public bool? ConfigManagerUserConfig { get; protected set; }
        /// <summary>
        /// Address or other identifying information to uniquely name the logical device.
        /// </summary>
        /// <remarks>CIM_Key</remarks>
        [libPlugNPlay.WMIProperty]
        public string CreationClassName { get; protected set; }
        /// <summary>
        /// Address or other identifying information to uniquely name the logical device.
        /// </summary>
        /// <remarks>CIM_Key</remarks>
        [libPlugNPlay.WMIProperty]
        public string DeviceID { get; protected set; }
        /// <summary>
        /// If TRUE, the error reported in the LastErrorCode property is now cleared.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public bool? ErrorCleared { get; protected set; }
        /// <summary>
        /// Free-form string that supplies information about the error recorded in the LastErrorCode property and corrective actions to perform.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public string ErrorDescription { get; protected set; }
        /// <summary>
        /// Last error code reported by the logical device.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public System.UInt32? LastErrorCode { get; protected set; }
        /// <summary>
        /// Indicates the Win32 Plug and Play device identifier of the logical device.
        /// </summary>
        /// <remarks>Schema ("Win32") </remarks>
        [libPlugNPlay.WMIProperty]
        public string PNPDeviceID { get; protected set; }
        /// <summary>
        /// Array of the specific power-related capabilities of a logical device. This property is inherited from CIM_LogicalDevice.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public libPlugNPlay.enumPowerManagementCapabilities[] PowerManagementCapabilities { get; protected set; }
        /// <summary>
        /// If TRUE, the device can be power managed, that is, put into a power-save state. If FALSE, the integer value 1 ("Not Supported") should be the only entry in the PowerManagementCapabilities array.
        /// <para />This property does not indicate whether power management features are currently enabled, or if enabled, which features are supported. For more information, see the PowerManagementCapabilities array.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public bool? PowerManagementSupported { get; protected set; }
        /// <summary>
        /// State of the logical device. If this property does not apply to the logical device, the value 5 ("Not Applicable") should be used.
        /// </summary>
        /// <remarks>MappingStrings ("MIF.DMTF|Operational State|003.3") </remarks>
        [libPlugNPlay.WMIProperty]
        public libPlugNPlay.enumStatusInfo? StatusInfo { get; protected set; }
        /// <summary>
        /// Scoping system's creation class name.
        /// </summary>
        /// <remarks>Propagated ("CIM_System.CreationClassName")</remarks>
        /// <remarks>CIM_Key</remarks>
        [libPlugNPlay.WMIProperty]
        public string SystemCreationClassName { get; protected set; }
        /// <summary>
        /// Scoping system's name.
        /// </summary>
        /// <remarks>Propagated ("CIM_System.Name")</remarks>
        /// <remarks>CIM_Key</remarks>
        [libPlugNPlay.WMIProperty]
        public string SystemName { get; protected set; }
        #endregion
    }

    /// <summary>
    /// The CIM_LogicalElement class is the base class for all system components that represent abstract system components, such as profiles, processes, or system capabilities, in the form of logical devices.
    /// <para />Important  The DMTF (Distributed Management Task Force) CIM (Common Information Model) classes are the parent classes upon which WMI classes are built. WMI currently supports only the CIM 2.x version schemas. 
    /// </summary>
    /// <remarks>WMI does not implement this class. For classes derived from CIM_LogicalElement, see Win32 Classes.</remarks>
    /// <remarks>This documentation is derived from the CIM class descriptions published by the DMTF. Microsoft may have made changes to correct minor errors, conform to Microsoft SDK documentation standards, or provide more information.</remarks>
    /// <remarks>Namespace: Root\CIMV2</remarks>
    /// <remarks>MOF: CIMWin32.mof</remarks>
    /// <remarks>DLL: CIMWin32.dll</remarks>
    public abstract class CIM_LogicalElement : CIM_ManagedSystemElement
    {
        public new const string UUID = "{8502C518-5FBB-11D2-AAC1-006008C78BC7}";
        public new const string WMI_Namespace = @"Root\CIMV2";
        public new const string WMI_ClassName = "CIM_LogicalElement";

        #region Properties
        #region inherited from CIM_ManagedSystemelement
        //string Caption;
        //string Description;
        //System.DateTime InstallDate;
        //string Name;
        //string Status;
        #endregion
        #endregion
    }
    #endregion

    #region physical
    /// <summary>
    /// The CIM_PhysicalComponent class represents a low-level or basic component within a package. A physical element that is not a link, connector, or package is a descendent (or member) of this class. For example, the UART chipset on an internal modem card would be a subclass (if additional properties or associations are defined) or an instance of CIM_PhysicalComponent.
    /// <para />Important  The DMTF (Distributed Management Task Force) CIM (Common Information Model) classes are the parent classes upon which WMI classes are built. WMI currently supports only the CIM 2.x version schemas.
    /// </summary>
    /// <remarks>The CIM_PhysicalComponent class is derived from CIM_PhysicalElement.
    /// <para />WMI does not implement this class. For WMI classes derived from CIM_PhysicalComponent, see Win32 Classes.</remarks>
    /// <remarks>Namespace: Root\CIMV2</remarks>
    /// <remarks>MOF: CIMWin32.mof</remarks>
    /// <remarks>DLL: CIMWin32.dll</remarks>
    public abstract class CIM_PhysicalComponent : CIM_PhysicalElement
    {
        public new const string UUID = "{FAF76B78-798C-11D2-AAD1-006008C78BC7}";
        public new const string WMI_Namespace = @"Root\CIMV2";
        public new const string WMI_ClassName = "CIM_PhysicalElement";

        #region Properties
        #region inherited from CIM_PhysicalElement
        #region inherited from CIM_ManagedSystemelement
        //string Caption;
        //string Description;
        //System.DateTime InstallDate;
        //string Name;
        //string Status;
        #endregion
        //string CreationClassName;
        //string Manufacturer;
        //string Model;
        //string OtherIdentifyingInfo;
        //string PartNumber;
        //bool? PoweredOn;
        //string SerialNumber;
        //string SKU;
        //string Tag;
        //string Version;
        #endregion
        /// <summary>
        /// If TRUE, the package can be hot-swapped. A physical package can be hot-swapped if the element can be replaced by a physically different (but equivalent) one while the containing package is turned on. For example, a fan component may be designed to be hot-swapped. All components that can be hot-swapped are inherently removable and replaceable.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public bool? HotSwappable { get; protected set; }
        /// <summary>
        /// If TRUE, this element is designed to be taken in and out of the physical container in which it is normally found, without impairing the function of the overall packaging. A package is considered removable even if the power must be off to perform the removal. If the power can be on and the package removed, then the element is removable and can be hot-swapped. For example, an upgradeable processor chip is removable.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public bool? Removable { get; protected set; }
        /// <summary>
        /// If TRUE, it is possible to replace the element with a physically different one. For example, some computer systems allow the main processor chip to be upgraded to one of a higher clock rating. In this case, the processor is said to be replaceable. All removable components are inherently replaceable.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public bool? Replaceable { get; protected set; }
        #endregion
    }

    /// <summary>
    /// The CIM_PhysicalElement subclasses define any component of a system that has a distinct physical identity. Instances of this class can be defined in terms of labels that can be physically attached to the object. All processes, files, and logical devices are not considered to be physical elements. For example, it is not possible to attach a label to a modem; it is only possible to attach a label to the card that implements the modem. The same card could also implement a LAN adapter.
    /// <para />Tangible managed system elements (usually hardware items) have a physical manifestation. A managed system element is not necessarily a discrete component. For example, it is possible for a single card (a type of physical element) to host more than one logical device. The card would be represented by a single physical element associated with multiple logical devices.
    /// <para />Important  The DMTF (Distributed Management Task Force) CIM (Common Information Model) classes are the parent classes upon which WMI classes are built. WMI currently supports only the CIM 2.x version schemas. 
    /// </summary>
    /// <remarks>WMI does not implement this class. For WMI classes derived from CIM_PhysicalElement, see Win32 Classes.
    /// <para />This documentation is derived from the CIM class descriptions published by the DMTF. Microsoft may have made changes to correct minor errors, conform to Microsoft SDK documentation standards, or provide more information.</remarks>
    /// <remarks>Namespace: Root\CIMV2</remarks>
    /// <remarks>MOF: CIMWin32.mof</remarks>
    /// <remarks>DLL: CIMWin32.dll</remarks>
    public abstract class CIM_PhysicalElement : CIM_ManagedSystemElement
    {
        public new const string UUID = "{FAF76B61-798C-11D2-AAD1-006008C78BC7}";
        public new const string WMI_Namespace = @"Root\CIMV2";
        public new const string WMI_ClassName = "CIM_PhysicalElement";

        #region Properties
        #region inherited from CIM_ManagedSystemelement
        //string Caption;
        //string Description;
        //System.DateTime InstallDate;
        //string Name;
        //string Status;
        #endregion
        /// <summary>
        /// Name of the class or subclass used in the creation of an instance. When used with other key properties of the class, this property allows all instances of the class and its subclasses to be uniquely identified.
        /// </summary>
        /// <remarks>CIM_Key</remarks>
        /// <remarks>MaxLen (256)</remarks>
        string CreationClassName;
        /// <summary>
        /// Name of the organization responsible for producing the physical element. For more information, see the Vendor property of CIM_Product.
        /// </summary>
        /// <remarks>MaxLen (256)</remarks>
        string Manufacturer;
        /// <summary>
        /// Name by which the physical element is generally known.
        /// </summary>
        /// <remarks>MaxLen (64)</remarks>
        string Model;
        /// <summary>
        /// Additional data, beyond asset tag information, that can be used to identify a physical element. One example is bar-code data that is associated with an element, which also has an asset tag. Note that if only bar-code data is available, and is unique and able to be used as an element key, this property would be null and the bar-code data would be used as the class key in the Tag property. 
        /// </summary>
        string OtherIdentifyingInfo;
        /// <summary>
        /// Part number assigned by the organization responsible for producing or manufacturing the physical element.
        /// </summary>
        /// <remarks>MaxLen (256)</remarks>
        string PartNumber;
        /// <summary>
        /// If TRUE, the physical element is powered on. Otherwise, it is currently off.
        /// </summary>
        bool? PoweredOn;
        /// <summary>
        /// Manufacturer-allocated number used to identify the physical element.
        /// </summary>
        /// <remarks>MaxLen (64)</remarks>
        string SerialNumber;
        /// <summary>
        /// Stock-keeping unit number for this physical element.
        /// </summary>
        /// <remarks>MaxLen (64)</remarks>
        string SKU;
        /// <summary>
        /// Arbitrary string that uniquely identifies the physical element and serves as the element's key. This property can contain information, such as asset tag or serial number data. The key for CIM_PhysicalElement is placed very high in the object hierarchy to independently identify the hardware or entity, regardless of physical placement in (or on) cabinets, adapters, and so on. For example, a removable component that can be hot-swapped can be taken from its containing (scoping) package and be temporarily unused. The object still continues to exist and can even be inserted into a different scoping container. The key for a physical element is an arbitrary string that is defined independently of placement or location-oriented hierarchy.
        /// </summary>
        /// <remarks>CIM_Key</remarks>
        /// <remarks>MaxLen (256)</remarks>
        string Tag;
        /// <summary>
        /// String that indicates the version of the physical element.
        /// </summary>
        /// <remarks>MaxLen (64)</remarks>
        string Version;
        #endregion
    }
    #endregion
    #endregion

    #region Base
    /// <summary>
    /// The CIM_Product class is a concrete class that represents a collection of physical elements, software features and, other products, acquired as a unit. Acquisition implies an agreement between the supplier and consumer, which can have implications on product licensing, support, and warranty.
    /// <para />Important  The DMTF (Distributed Management Task Force) CIM (Common Information Model) classes are the parent classes upon which WMI classes are built. WMI currently supports only the CIM 2.x version schemas.
    /// </summary>
    /// <remarks>WMI does not implement this class. For WMI classes that are derived from CIM_Product, see Win32 Classes.
    /// <para />This documentation is derived from the CIM class descriptions published by the DMTF. Microsoft may have made changes to correct minor errors, conform to Microsoft SDK documentation standards, or provide more information.</remarks>
    /// <remarks>Namespace: Root\CIMV2</remarks>
    /// <remarks>MOF: CIMWin32.mof</remarks>
    /// <remarks>DLL: CIMWin32.dll</remarks>
    public abstract class CIM_Product
    {
        public new const string UUID = "{FAF76B63-798C-11D2-AAD1-006008C78BC7}";
        public new const string WMI_Namespace = @"Root\CIMV2";
        public new const string WMI_ClassName = "CIM_Product";

        #region Properties
        /// <summary>
        /// Short textual description of the current object.
        /// </summary>
        /// <remarks>MaxLen (64)</remarks>
        [libPlugNPlay.WMIProperty]
        public string Caption { get; private set; }
        /// <summary>
        /// Textual description of the current object.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public string Description { get; private set; }
        /// <summary>
        /// Identifier by which the current object is known.
        /// </summary>
        /// <remarks>MaxLen (256)</remarks>
        [libPlugNPlay.WMIProperty]
        public string SettingID { get; private set; }
        #endregion
    }

    /// <summary>
    /// The CIM_Setting class represents configuration-related and operational parameters for one or more managed system elements. A managed system element can have multiple setting objects associated with it. The current operational values for an element's parameters are reflected by properties in the element itself or by properties in its associations. These properties do not have to be the same values present in the setting object. For example, a modem may have a setting baud rate of 56 kilobytes per second, but be operating at 19.2 kilobytes per second.
    /// <para />Important  The DMTF (Distributed Management Task Force) CIM (Common Information Model) classes are the parent classes upon which WMI classes are built. WMI currently supports only the CIM 2.x version schemas.
    /// </summary>
    /// <remarks>The CIM_CDROMDrive class is derived from CIM_MediaAccessDevice.
    /// <para />WMI does not implement this class. For more information about classes derived from CIM_CDROMDrive, see Win32 Classes.
    /// <para />This documentation is derived from the CIM class descriptions published by the DMTF. Microsoft may have made changes to correct minor errors, conform to Microsoft SDK documentation standards, or provide more information.</remarks>
    /// <remarks>Namespace: Root\CIMV2</remarks>
    /// <remarks>MOF: CIMWin32.mof</remarks>
    /// <remarks>DLL: CIMWin32.dll</remarks>
    public abstract class CIM_Setting
    {
        public new const string UUID = "{8502C572-5FBB-11D2-AAC1-006008C78BC7}";
        public new const string WMI_Namespace = @"Root\CIMV2";
        public new const string WMI_ClassName = "CIM_Setting";

        #region Properties
        /// <summary>
        /// Short textual description of the current object.
        /// </summary>
        /// <remarks>MaxLen (64)</remarks>
        [libPlugNPlay.WMIProperty]
        public string Caption { get; private set; }
        /// <summary>
        /// Textual description of the current object.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public string Description { get; private set; }
        /// <summary>
        /// Identifier by which the current object is known.
        /// </summary>
        /// <remarks>MaxLen (256)</remarks>
        [libPlugNPlay.WMIProperty]
        public string SettingID { get; private set; }
        #endregion
    }

    /// <summary>
    /// The CIM_ManagedSystemElement class is the base class for the system element hierarchy. Any distinguishable system component is a candidate for inclusion in this class. Examples include software components, such as files; devices, such as disk drives and controllers; and physical components, such as chips and cards.
    /// <par />Important  The DMTF (Distributed Management Task Force) CIM (Common Information Model) classes are the parent classes upon which WMI classes are built. WMI currently supports only the CIM 2.x version schemas.
    /// </summary>
    /// <remarks>WMI does not implement this class. For WMI classes derived from this class, see Win32 Classes.
    /// <para />This documentation is derived from the CIM class descriptions published by the DMTF. Microsoft may have made changes to correct minor errors, conform to Microsoft SDK documentation standards, or provide more information.</remarks>
    /// <remarks>Namespace: Root\CIMV2</remarks>
    /// <remarks>MOF: CIMWin32.mof</remarks>
    /// <remarks>DLL: CIMWin32.dll</remarks>
    public abstract class CIM_ManagedSystemElement
    {
        public new const string UUID = "{8502C517-5FBB-11D2-AAC1-006008C78BC7}";
        public new const string WMI_Namespace = @"Root\CIMV2";
        public new const string WMI_ClassName = "CIM_ManagedSystemElement";

        #region Properties
        /// <summary>
        /// A short textual description of the object. 
        /// </summary>
        /// <remarks>MaxLen(64)</remarks>
        /// <remarks>DisplayName ("Caption")</remarks>
        [libPlugNPlay.WMIProperty]
        public string Caption { get; protected set; }
        /// <summary>
        /// A textual description of the object. 
        /// </summary>
        /// <remarks>DisplayName ("Description")</remarks>
        [libPlugNPlay.WMIProperty]
        public string Description { get; protected set; }
        /// <summary>
        /// Indicates when the object was installed. Lack of a value does not indicate that the object is not installed.
        /// </summary>
        /// <remarks>MappingStrings ("MIF.DMTF|ComponentID|001.5")</remarks>
        /// <remarks>DisplayName ("Install Date")</remarks>
        [libPlugNPlay.WMIProperty]
        public System.DateTime InstallDate { get; protected set; }
        /// <summary>
        /// Label by which the object is known. When subclassed, this property can be overridden to be a key property.
        /// </summary>
        /// <remarks>DisplayName ("Name")</remarks>
        [libPlugNPlay.WMIProperty]
        public string Name { get; protected set; }
        /// <summary>
        /// String that indicates the current status of the object. Operational and non-operational status can be defined. Operational status can include "OK", "Degraded", and "Pred Fail". "Pred Fail" indicates that an element is functioning properly, but is predicting a failure (for example, a SMART-enabled hard disk drive).
        /// <para />Non-operational status can include "Error", "Starting", "Stopping", and "Service". "Service" can apply during disk mirror-resilvering, reloading a user permissions list, or other administrative work. Not all such work is online, but the managed element is neither "OK" nor in one of the other states.
        /// </summary>
        /// <remarks>MaxLen(10)</remarks>
        /// <remarks>DisplayName ("Status")</remarks>
        /// <see cref="dictionaryStatus"/>
        [libPlugNPlay.WMIProperty]
        public string Status { get; protected set; }
        #endregion
    }
    #endregion







}