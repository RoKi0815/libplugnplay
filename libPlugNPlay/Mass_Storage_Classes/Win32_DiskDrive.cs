﻿namespace libPlugNPlay
{
    /// <summary>
    /// The Win32_DiskDrive WMI class represents a physical disk drive as seen by a computer running the Windows operating system. 
    /// </summary>
    /// <remarks><para>Physical hard disk drives are the primary storage medium for information in any computing environment. Although organizations often use devices such as tape drives and compact disc drives for archiving data, these devices are not suited for day-to-day storage of user data. Only physical hard disks offer the speed and ease of use required for storing data and for running applications and the operating system.</para>
    /// <para>To efficiently manage data, it is important to have a detailed inventory of all your physical disks, their capabilities, and their capacities. You can use the Win32_DiskDrive class to derive this type of inventory.</para>
    /// <para>Any interface to a Windows physical disk drive is a descendent (or member) of this class. The features of the disk drive seen through this object correspond to the logical and management characteristics of the drive. In some cases, this may not reflect the actual physical characteristics of the device. Any object based on another logical device would not be a member of this class.</para>
    /// <para>For security reasons, a user connecting from a remote computer must have the SC_MANAGER_CONNECT privilege enabled to be able to enumerate this class. For more information, see Service Security and Access Rights.</para>
    /// <para>The Win32_DiskDrive class is derived from CIM_DiskDrive which derives from CIM_MediaAccessDevice. The CIM_MediaAccessDevice class derives from CIM_LogicalDevice.</para></remarks>
    /// <remarks>Namespace: Root\CIMV2</remarks>
    /// <remarks>MOF: CIMWin32.mof</remarks>
    /// <remarks>DLL: CIMWin32.dll</remarks>
    public class Win32_DiskDrive : Win32_PnPEntity
    {
        public new const string UUID = "{8502C4B2-5FBB-11D2-AAC1-006008C78BC7}";
        public new const string WMI_Namespace = @"Root\CIMV2";
        public new const string WMI_ClassName = "Win32_DiskDrive";
        public new static readonly System.Management.ManagementClass WMI_ClassReference = new System.Management.ManagementClass(@"\\.\" + WMI_Namespace + ":" + WMI_ClassName);

        #region methods
        #region from Win32_DiskDrive
        /// <summary>
        /// Not implemented. To implement this method, see the Reset method in CIM_DiskDrive for documentation.
        /// </summary>
        [libPlugNPlay.WMIMethod]
        public System.UInt32 Reset()
        {
            throw new System.NotImplementedException("This method is currently not implemented by WMI. To use this method, you must implement it in your own provider.");
        }
        /// <summary>
        /// Not implemented. To implement this method, see the SetPowerState method in CIM_DiskDrive for documentation.
        /// </summary>
        [libPlugNPlay.WMIMethod]
        public System.UInt32 SetPowerState(libPlugNPlay.enumPowerStateValueMap PowerState, System.DateTime Time)
        {
            throw new System.NotImplementedException("This method is currently not implemented by WMI. To use this method, you must implement it in your own provider.");
        }
        #endregion
        #endregion

        #region properties
        #region from Win32_DiskDrive
        #region inherited from Win32_PnPEntity (CIM_ManagedSystemElement, CIM_LogicalDevice)
        //enumAvailability? Availability;
        //string Caption;
        //enumConfigManagerErrorCode? ConfigManagerErrorCode;
        //bool? ConfigManagerUserConfig;
        //string CreationClassName;
        //string Description;
        //string DeviceID;
        //bool? ErrorCleared;
        //string ErrorDescription;
        //System.DateTime InstallDate;
        //System.UInt32? LastErrorCode;
        //string Manufacturer;
        //string Name;
        //string PNPDeviceID;
        //enumPowerManagementCapabilities[] PowerManagementCapabilities;
        //bool? PowerManagementSupported;
        //string Status;
        //enumStatusInfo? StatusInfo;
        //string SystemCreationClassName;
        //string SystemName;
        #endregion
        /// <summary>
        /// Number of bytes in each sector for the physical disk drive.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public System.UInt32? BytesPerSector { get; protected set; }
        /// <summary>
        /// Array of capabilities of the media access device. For example, the device may support random access (3), removable media (7), and automatic cleaning (9). This property is inherited from CIM_MediaAccessDevice.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public emunCapabilities[] Capabilities { get; protected set; }
        /// <summary>
        /// List of more detailed explanations for any of the access device features indicated in the Capabilities array. Note, each entry of this array is related to the entry in the Capabilities array that is located at the same index. This property is inherited from CIM_MediaAccessDevice.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public string[] CapabilityDescriptions { get; protected set; }
        /// <summary>
        /// Algorithm or tool used by the device to support compression. This property is inherited from CIM_MediaAccessDevice.
        /// <para />The name of the compression algorithm or one of the following values <see cref="dictionaryCompressionMethod"/>.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public string CompressionMethod { get; protected set; }
        /// <summary>
        /// Default block size, in bytes, for this device. This property is inherited from CIM_MediaAccessDevice.
        /// <para />For more information about using uint64 values in scripts, see Scripting in WMI.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public System.UInt64? DefaultBlockSize { get; protected set; }
        /// <summary>
        /// Type of error detection and correction supported by this device. This property is inherited from CIM_MediaAccessDevice.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public string ErrorMethodology { get; protected set; }
        /// <summary>
        /// Revision for the disk drive firmware that is assigned by the manufacturer.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public string FirmwareRevision { get; protected set; }
        /// <summary>
        /// Physical drive number of the given drive. This property is filled by the GetDriveMapInfo method. A value of 0xFF indicates that the given drive does not map to a physical drive.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public System.UInt32? Index { get; protected set; }
        /// <summary>
        /// Interface type of physical disk drive.
        /// </summary>
        /// <remarks><see cref="dictionaryInterfaceType"/></remarks>
        [libPlugNPlay.WMIProperty]
        public string InterfaceType { get; protected set; }
        /// <summary>
        /// Maximum block size, in bytes, for media accessed by this device. This property is inherited from CIM_MediaAccessDevice.
        /// <para />For more information about using uint64 values in scripts, see Scripting in WMI.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public System.UInt64? MaxBlockSize { get; protected set; }
        /// <summary>
        /// Maximum media size, in kilobytes, of media supported by this device. This property is inherited from CIM_MediaAccessDevice.
        /// <para />For more information about using uint64 values in scripts, see Scripting in WMI.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public System.UInt64? MaxMediaSize { get; protected set; }
        /// <summary>
        /// If True, the media for a disk drive is loaded, which means that the device has a readable file system and is accessible. For fixed disk drives, this property will always be TRUE.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public bool? MediaLoaded { get; protected set; }
        /// <summary>
        /// Type of media used or accessed by this device.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public string MediaType { get; protected set; }
        /// <summary>
        /// Minimum block size, in bytes, for media accessed by this device. This property is inherited from CIM_MediaAccessDevice.
        /// <para />For more information about using uint64 values in scripts, see Scripting in WMI.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public System.UInt64? MinBlockSize { get; protected set; }
        /// <summary>
        /// Manufacturer's model number of the disk drive.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public string Model { get; protected set; }
        /// <summary>
        /// If True, the media access device needs cleaning. Whether manual or automatic cleaning is possible is indicated in the Capabilities property. This property is inherited from CIM_MediaAccessDevice.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public bool? NeedsCleaning { get; protected set; }
        /// <summary>
        /// Maximum number of media which can be supported or inserted (when the media access device supports multiple individual media). This property is inherited from CIM_MediaAccessDevice.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public System.UInt32? NumberOfMediaSupported { get; protected set; }
        /// <summary>
        /// Number of partitions on this physical disk drive that are recognized by the operating system.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public System.UInt32? Partitions { get; protected set; }
        /// <summary>
        /// SCSI bus number of the disk drive.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public System.UInt32? SCSIBus { get; protected set; }
        /// <summary>
        /// SCSI logical unit number (LUN) of the disk drive.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public System.UInt16? SCSILogicalUnit { get; protected set; }
        /// <summary>
        /// SCSI port number of the disk drive.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public System.UInt16? SCSIPort { get; protected set; }
        /// <summary>
        /// SCSI identifier number of the disk drive.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public System.UInt16? SCSITargetId { get; protected set; }
        /// <summary>
        /// Number of sectors in each track for this physical disk drive.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public System.UInt32? SectorsPerTrack { get; protected set; }
        /// <summary>
        /// Number allocated by the manufacturer to identify the physical media. 
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public string SerialNumber { get; protected set; }
        /// <summary>
        /// Disk identification. This property can be used to identify a shared resource.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public System.UInt32? Signature { get; protected set; }
        /// <summary>
        /// Size of the disk drive. It is calculated by multiplying the total number of cylinders, tracks in each cylinder, sectors in each track, and bytes in each sector.
        /// <para />For more information about using uint64 values in scripts, see Scripting in WMI.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public System.UInt64? Size { get; protected set; }
        /// <summary>
        /// Total number of cylinders on the physical disk drive. Note: the value for this property is obtained through extended functions of BIOS interrupt 13h. The value may be inaccurate if the drive uses a translation scheme to support high-capacity disk sizes. Consult the manufacturer for accurate drive specifications.
        /// <para />For more information about using uint64 values in scripts, see Scripting in WMI.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public System.UInt64? TotalCylinders { get; protected set; }
        /// <summary>
        /// Total number of heads on the disk drive. Note: the value for this property is obtained through extended functions of BIOS interrupt 13h. The value may be inaccurate if the drive uses a translation scheme to support high-capacity disk sizes. Consult the manufacturer for accurate drive specifications.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public System.UInt32? TotalHeads { get; protected set; }
        /// <summary>
        /// Total number of sectors on the physical disk drive. Note: the value for this property is obtained through extended functions of BIOS interrupt 13h. The value may be inaccurate if the drive uses a translation scheme to support high-capacity disk sizes. Consult the manufacturer for accurate drive specifications.
        /// <para />For more information about using uint64 values in scripts, see Scripting in WMI.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public System.UInt64? TotalSectors { get; protected set; }
        /// <summary>
        /// Total number of tracks on the physical disk drive. Note: the value for this property is obtained through extended functions of BIOS interrupt 13h. The value may be inaccurate if the drive uses a translation scheme to support high-capacity disk sizes. Consult the manufacturer for accurate drive specifications.
        /// <para />For more information about using uint64 values in scripts, see Scripting in WMI.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public System.UInt64? TotalTracks { get; protected set; }
        /// <summary>
        /// Number of tracks in each cylinder on the physical disk drive. Note: the value for this property is obtained through extended functions of BIOS interrupt 13h. The value may be inaccurate if the drive uses a translation scheme to support high-capacity disk sizes. Consult the manufacturer for accurate drive specifications.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public System.UInt32? TracksPerCylinder { get; protected set; }
        #endregion
        #endregion

        public Win32_DiskDrive(System.Management.ManagementObject RefObj)
            : base(RefObj)
        {
            //Win32_DiskDrive.FillPropertiesFromMBO(this, RefObj);
        }

        protected new virtual void InitValues()
        {
            base.InitValues();
            //nop
        }
    }
}
