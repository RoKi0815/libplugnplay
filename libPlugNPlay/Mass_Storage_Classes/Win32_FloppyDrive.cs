﻿namespace libPlugNPlay
{
    /// <summary>
    /// The Win32_FloppyDrive WMI class manages the functions of a floppy disk drive.
    /// </summary>
    /// <remarks>The Win32_FloppyDrive class is derived from CIM_DisketteDrive which derives from CIM_MediaAccessDevice. The CIM_MediaAccessDevice class derives from CIM_LogicalDevice.</remarks>
    /// <remarks>Namespace: Root\CIMV2</remarks>
    /// <remarks>MOF: CIMWin32.mof</remarks>
    /// <remarks>DLL: CIMWin32.dll</remarks>
    [System.Obsolete("Win32_FloppyDrive is no longer available for use as of Windows 10 and Windows Server 2016 Technical Preview.")]
    public class Win32_FloppyDrive : Win32_PnPEntity
    {
        public new const string UUID = "{FB1F3A64-BBAC-11d2-85E5-0000F8102E5F}";
        public new const string WMI_Namespace = @"Root\CIMV2";
        public new const string WMI_ClassName = "Win32_FloppyDrive";
        public new static readonly System.Management.ManagementClass WMI_ClassReference = new System.Management.ManagementClass(@"\\.\" + WMI_Namespace + ":" + WMI_ClassName);

        #region methods
        #region from Win32_FloppyDrive
        /// <summary>
        /// Not implemented. For more information about how to implement this method, see the Reset method in CIM_DisketteDrive.
        /// </summary>
        [libPlugNPlay.WMIMethod]
        public System.UInt32 Reset()
        {
            throw new System.NotImplementedException("This method is currently not implemented by WMI. To use this method, you must implement it in your own provider.");
        }
        /// <summary>
        /// Not implemented. For more information about how to implement this method, see the SetPowerState method in CIM_DisketteDrive.
        /// </summary>
        [libPlugNPlay.WMIMethod]
        public System.UInt32 SetPowerState(libPlugNPlay.enumPowerStateValueMap PowerState, System.DateTime Time)
        {
            throw new System.NotImplementedException("This method is currently not implemented by WMI. To use this method, you must implement it in your own provider.");
        }
        #endregion
        #endregion

        #region properties
        #region from Win32_FloppyDrive
        #region inherited from Win32_PnPEntity (CIM_ManagedSystemElement, CIM_LogicalDevice)
        //enumAvailability? Availability;
        //string Caption;
        //enumConfigManagerErrorCode? ConfigManagerErrorCode;
        //bool? ConfigManagerUserConfig;
        //string CreationClassName;
        //string Description;
        //string DeviceID;
        //bool? ErrorCleared;
        //string ErrorDescription;
        //System.DateTime InstallDate;
        //System.UInt32? LastErrorCode;
        //string Manufacturer;
        //string Name;
        //string PNPDeviceID;
        //enumPowerManagementCapabilities[] PowerManagementCapabilities;
        //bool? PowerManagementSupported;
        //string Status;
        //enumStatusInfo? StatusInfo;
        //string SystemCreationClassName;
        //string SystemName;
        #endregion

        /// <summary>
        /// Array of capabilities of the media access device. For example, the device may support Random Access, Removable Media, and Automatic Cleaning. In this case, the values 3, 7, and 9 would be written to the array. This property is inherited from CIM_MediaAccessDevice.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public emunCapabilities[] Capabilities { get; protected set; }
        /// <summary>
        /// Array of free-form strings providing more detailed explanations for any of the serial controller features indicated in the Capabilities array. Note, each entry of this array is related to the entry in the Capabilities array that is located at the same index. This property is inherited from CIM_MediaAccessDevice.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public string[] CapabilityDescriptions { get; protected set; }
        /// <summary>
        /// Free form string that indicates the algorithm or tool used by the device to support compression.
        /// <para />If it is not possible to describe the compression scheme (for example, because it is unknown), use the following:
        /// <para />"Unknown" to represent that it is unknown whether the device supports compression capabilities.
        /// <para />"Compressed" to represent that the device supports compression capabilities, but its compression scheme is either unknown or undisclosed.
        /// <para />"Not Compressed" to represent that the devices does not support compression capabilities.
        /// <para />This property is inherited from CIM_MediaAccessDevice.
        /// </summary>
        /// <remarks>dictionaryCompressionMethod</remarks>
        [libPlugNPlay.WMIProperty]
        public string CompressionMethod { get; protected set; }
        /// <summary>
        /// Default block size, in bytes, for the device. This property is inherited
        /// <para />For more information about using uint64 values in scripts, see Scripting in WMI.
        /// <para />This Property is inherited from CIM_MediaAccessDevice.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public System.UInt64? DefaultBlockSize { get; protected set; }
        /// <summary>
        /// Free-form string that describes the type(s) of error detection and correction supported by the device. This property is inherited from CIM_MediaAccessDevice.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public string ErrorMethodology { get; protected set; }
        /// <summary>
        /// Maximum block size, in bytes, for media accessed by the device. This property is inherited from CIM_MediaAccessDevice.
        /// <para />For more information about using uint64 values in scripts, see Scripting in WMI.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public System.UInt64? MaxBlockSize { get; protected set; }
        /// <summary>
        /// Maximum size, in kilobytes, of media supported by the device. Kilobytes is interpreted as the number of bytes multiplied by 1000 (not the number of bytes multiplied by 1024). This property is inherited from CIM_MediaAccessDevice.
        /// <para />For more information about using uint64 values in scripts, see Scripting in WMI.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public System.UInt64? MaxMediaSize { get; protected set; }
        /// <summary>
        /// Minimum block size, in bytes, for media accessed by the device. This property is inherited from CIM_MediaAccessDevice.
        /// <para />For more information about using uint64 values in scripts, see Scripting in WMI.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public System.UInt64? MinBlockSize { get; protected set; }
        /// <summary>
        /// If TRUE, the media access device needs cleaning. Manual or automatic cleaning is indicated in the Capabilities array property. This property is inherited from CIM_MediaAccessDevice.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public bool? NeedsCleaning { get; protected set; }
        /// <summary>
        /// When the media access device supports multiple individual media, this property defines the maximum number that can be supported or inserted. This property is inherited from CIM_MediaAccessDevice.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public System.UInt32? NumberOfMediaSupported { get; protected set; }
        /// <summary>
        /// Firmware revision level of the Windows CD-ROM drive.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public string RevisionLevel { get; protected set; }
        #endregion
        #endregion

        public Win32_FloppyDrive(System.Management.ManagementObject RefObj)
            : base(RefObj)
        { }

        protected new virtual void InitValues()
        {
            base.InitValues();
            //nop
        }
    }
}
