﻿namespace libPlugNPlay
{
    /// <summary>
    /// The Win32_PhysicalMedia class represents any type of documentation or storage medium, such as tapes, CD ROMs, and so on. To obtain the characteristics of the media in a CD drive, such as whether it is writeable, use Win32_CDROMDrive and the Capabilities property.
    /// </summary>
    /// <remarks>Namespace: Root\CIMV2</remarks>
    /// <remarks>MOF: Wmipcima.mof</remarks>
    /// <remarks>DLL: Wmipcima.dll</remarks>
    public class Win32_PhysicalMedia:Win32_PnPEntity
    {
        public new const string UUID = "{BF253431-1E4D-4F57-00E7-64B2CACC801E}";
        public new const string WMI_Namespace = @"Root\CIMV2";
        public new const string WMI_ClassName = "Win32_PhysicalMedia";
        public new static readonly System.Management.ManagementClass WMI_ClassReference = new System.Management.ManagementClass(@"\\.\" + Win32_PhysicalMedia.WMI_Namespace + ":" + Win32_PhysicalMedia.WMI_ClassName);

        #region properties
        #region from Win32_PhysicalMedia
        #region inherited from Win32_PnPEntity (CIM_ManagedSystemElement, CIM_LogicalDevice)
        //string Caption;
        //string CreationClassName;
        //string Description;
        //System.DateTime InstallDate;
        //string Manufacturer;
        //string Name;
        //string Status;
        #region not existing
        /// <summary>
        /// Not existing in Win32_PhysicalMedia.
        /// </summary>
        [libPlugNPlay.WMIProperty, libPlugNPlay.WMINotExisting]
        private new enumAvailability? Availability { get; set; }
        /// <summary>
        /// Not existing in Win32_PhysicalMedia.
        /// </summary>
        [libPlugNPlay.WMIProperty, libPlugNPlay.WMINotExisting]
        private new enumConfigManagerErrorCode? ConfigManagerErrorCode { get; set; }
        /// <summary>
        /// Not existing in Win32_PhysicalMedia.
        /// </summary>
        [libPlugNPlay.WMIProperty, libPlugNPlay.WMINotExisting]
        private new bool? ConfigManagerUserConfig { get; set; }
        /// <summary>
        /// Not existing in Win32_PhysicalMedia.
        /// </summary>
        [libPlugNPlay.WMIProperty, libPlugNPlay.WMINotExisting]
        private new string DeviceID { get; set; }
        /// <summary>
        /// Not existing in Win32_PhysicalMedia.
        /// </summary>
        [libPlugNPlay.WMIProperty, libPlugNPlay.WMINotExisting]
        private new bool? ErrorCleared { get; set; }
        /// <summary>
        /// Not existing in Win32_PhysicalMedia.
        /// </summary>
        [libPlugNPlay.WMIProperty, libPlugNPlay.WMINotExisting]
        private new string ErrorDescription { get; set; }
        /// <summary>
        /// Not existing in Win32_PhysicalMedia.
        /// </summary>
        [libPlugNPlay.WMIProperty, libPlugNPlay.WMINotExisting]
        private new System.UInt32? LastErrorCode { get; set; }
        /// <summary>
        /// Not existing in Win32_PhysicalMedia.
        /// </summary>
        [libPlugNPlay.WMIProperty, libPlugNPlay.WMINotExisting]
        private new string PNPDeviceID { get; set; }
        /// <summary>
        /// Not existing in Win32_PhysicalMedia.
        /// </summary>
        [libPlugNPlay.WMIProperty, libPlugNPlay.WMINotExisting]
        private new enumPowerManagementCapabilities[] PowerManagementCapabilities { get; set; }
        /// <summary>
        /// Not existing in Win32_PhysicalMedia.
        /// </summary>
        [libPlugNPlay.WMIProperty, libPlugNPlay.WMINotExisting]
        private new bool? PowerManagementSupported { get; set; }
        /// <summary>
        /// Not existing in Win32_PhysicalMedia.
        /// </summary>
        [libPlugNPlay.WMIProperty, libPlugNPlay.WMINotExisting]
        private new enumStatusInfo? StatusInfo { get; set; }
        /// <summary>
        /// Not existing in Win32_PhysicalMedia.
        /// </summary>
        [libPlugNPlay.WMIProperty, libPlugNPlay.WMINotExisting]
        private new string SystemCreationClassName { get; set; }
        /// <summary>
        /// Not existing in Win32_PhysicalMedia.
        /// </summary>
        [libPlugNPlay.WMIProperty, libPlugNPlay.WMINotExisting]
        private new string SystemName { get; set; }
        #endregion
        #endregion

        /// <summary>
        /// Number of bytes that can be read from, or written to, a media. This property is not applicable to hard copy (documentation) or cleaner media. Data compression should not be assumed, as it would increase the value in this property. For tapes, it should be assumed that no file marks or blank space areas are recorded on the media.
        /// <para />For more information about using uint64 values in scripts, see Scripting in WMI.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public System.UInt64? Capacity { get; protected set; }
        /// <summary>
        /// If TRUE, the physical media is used for cleaning purposes, not data storage.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public bool? CleanerMedia { get; protected set; }
        /// <summary>
        /// If TRUE, the package can be hot-swapped. A physical package can be hot-swapped if the element can be replaced by a physically different (but equivalent) one while the containing package is turned on. For example, a fan component may be designed to be hot-swapped. All components that can be hot-swapped are inherently removable and replaceable. This property is inherited from CIM_PhysicalComponent.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public bool? HotSwappable { get; protected set; }
        /// <summary>
        /// Additional detail related to the MediaType enumeration. For example, if value 3 ("QIC Cartridge") is specified, this property would indicate whether the tape is wide or 1/4 inch, pre-formatted, Travan compatible, and so on.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public string MediaDescription { get; protected set; }
        /// <summary>
        /// Type of physical media. The MediaDescription property provides more explicit definition of the media type.
        /// </summary>
        /// <remarks><see cref="dictionaryMediaType"/></remarks>
        [libPlugNPlay.WMIProperty]
        public enumMediaType? MediaType { get; protected set; }
        /// <summary>
        /// Name by which the physical element is generally known. This property is inherited from CIM_PhysicalElement.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public string Model { get; protected set; }
        /// <summary>
        /// Additional data, beyond asset tag information, that can be used to identify a physical element. One example is bar-code data that is associated with an element, which also has an asset tag. Note that if only bar-code data is available, and is unique and able to be used as an element key, this property would be null and the bar-code data would be used as the class key in the Tag property. This property is inherited from CIM_PhysicalElement.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public string OtherIdentifyingInfo { get; protected set; }
        /// <summary>
        /// Part number assigned by the organization responsible for producing or manufacturing the physical element. This property is inherited from CIM_PhysicalElement.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public string PartNumber { get; protected set; }
        /// <summary>
        /// If TRUE, the physical element is powered on. This property is inherited from CIM_PhysicalElement.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public bool? PoweredOn { get; protected set; }
        /// <summary>
        /// If TRUE, this element is designed to be taken in and out of the physical container in which it is normally found, without impairing the function of the overall packaging. A package is considered removable even if the power must be off to perform the removal. If the power can be on and the package removed, then the element is removable and can be hot-swapped. For example, an ungradable processor chip is removable. This property is inherited from CIM_PhysicalComponent.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public bool? Removable { get; protected set; }
        /// <summary>
        /// If TRUE, it is possible to replace the element with a physically different one. For example, some computer systems allow the main processor chip to be upgraded to one of a higher clock rating. In this case, the processor is said to be replaceable. All removable components are inherently replaceable. This property is inherited from CIM_PhysicalComponent.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public bool? Replaceable { get; protected set; }
        /// <summary>
        /// Manufacturer-allocated number used to identify the physical element. This property is inherited from CIM_PhysicalElement.
        /// </summary>
        /// <remarks>Win32_PhysicalMedia: default value = NULL</remarks>
        [libPlugNPlay.WMIProperty]
        public string SerialNumber{ get; protected set; }
        /// <summary>
        /// Stock-keeping unit number for the physical element. This property is inherited from CIM_PhysicalElement.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public string SKU { get; protected set; }
        /// <summary>
        /// Arbitrary string that uniquely identifies the physical element and serves as the element's key. This property can contain information, such as asset tag or serial number data. The key for CIM_PhysicalElement is placed very high in the object hierarchy to independently identify the hardware or entity, regardless of physical placement in (or on) cabinets, adapters, and so on. For example, a removable component that can be hot-swapped can be taken from its containing (scoping) package and be temporarily unused. The object still continues to exist and can even be inserted into a different scoping container. The key for a physical element is an arbitrary string that is defined independently of placement or location-oriented hierarchy. This property is inherited from CIM_PhysicalElement.
        /// </summary>
        /// <remarks>Win32_PhysicalMedia: default value = NULL</remarks>
        [libPlugNPlay.WMIProperty]
        public string Tag { get; protected set; }
        /// <summary>
        /// Version of the physical element. This property is inherited from CIM_PhysicalElement.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public string Version { get; protected set; }
        /// <summary>
        /// If TRUE, the media is currently write-protected by a physical mechanism, such as a protect tab on a floppy disk.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public bool? WriteProtectOn { get; protected set; }
        #endregion
        #endregion

        public Win32_PhysicalMedia(System.Management.ManagementObject RefObj)
            : base(RefObj)
        {
        }

        protected new virtual void InitValues()
        {
            base.InitValues();
            this.SerialNumber = null;
            this.Tag = null;
        }
    }
}
