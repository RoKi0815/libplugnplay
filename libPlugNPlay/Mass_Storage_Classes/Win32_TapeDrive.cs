﻿namespace libPlugNPlay
{
    /// <summary>
    /// The Win32_TapeDrive WMI class represents a tape drive on a computer system running Windows. Tape drives are primarily distinguished by the fact that they can only be accessed sequentially.
    /// </summary>
    /// <remarks>The Win32_TapeDrive class is derived from CIM_TapeDrive.</remarks>
    /// <remarks>Namespace: Root\CIMV2</remarks>
    /// <remarks>MOF: CIMWin32.mof</remarks>
    /// <remarks>DLL: CIMWin32.dll</remarks>
    public class Win32_TapeDrive : Win32_PnPEntity
    {
        public new const string UUID = "{8502C4B1-5FBB-11D2-AAC1-006008C78BC7}";
        public new const string WMI_Namespace = @"Root\CIMV2";
        public new const string WMI_ClassName = "Win32_TapeDrive";
        public new static readonly System.Management.ManagementClass WMI_ClassReference = new System.Management.ManagementClass(@"\\.\" + WMI_Namespace + ":" + WMI_ClassName);

        #region methods
        #region from Win32_TapeDrive
        /// <summary>
        /// Not implemented. To implement this method, see the Reset method in CIM_TapeDrive.
        /// </summary>
        [libPlugNPlay.WMIMethod]
        public System.UInt32 Reset()
        {
            throw new System.NotImplementedException("This method is currently not implemented by WMI. To use this method, you must implement it in your own provider.");
        }
        /// <summary>
        /// Not implemented. To implement this method, see the SetPowerState method in CIM_TapeDrive.
        /// </summary>
        [libPlugNPlay.WMIMethod]
        public System.UInt32 SetPowerState(libPlugNPlay.enumPowerStateValueMap PowerState, System.DateTime Time)
        {
            throw new System.NotImplementedException("This method is currently not implemented by WMI. To use this method, you must implement it in your own provider.");
        }
        #endregion
        #endregion

        #region properties
        #region from Win32_TapeDrive
        #region inherited from Win32_PnPEntity (CIM_ManagedSystemElement, CIM_LogicalDevice)
        //enumAvailability? Availability;
        //string Caption;
        //enumConfigManagerErrorCode? ConfigManagerErrorCode;
        //bool? ConfigManagerUserConfig;
        //string CreationClassName;
        //string Description;
        //string DeviceID;
        //bool? ErrorCleared;
        //string ErrorDescription;
        //System.DateTime InstallDate;
        //System.UInt32? LastErrorCode;
        //string Manufacturer;
        //string Name;
        //string PNPDeviceID;
        //enumPowerManagementCapabilities[] PowerManagementCapabilities;
        //bool? PowerManagementSupported;
        //string Status;
        //enumStatusInfo? StatusInfo;
        //string SystemCreationClassName;
        //string SystemName;
        #endregion

        /// <summary>
        /// Array of capabilities of the media access device. For example, the device may support Random Access, Removable Media, and Automatic Cleaning. In this case, the values 3, 7, and 9 would be written to the array. This property is inherited from CIM_MediaAccessDevice.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public emunCapabilities[] Capabilities { get; protected set; }
        /// <summary>
        /// Array of free-form strings providing more detailed explanations for any of the serial controller features indicated in the Capabilities array. Note, each entry of this array is related to the entry in the Capabilities array that is located at the same index. This property is inherited from CIM_MediaAccessDevice.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public string[] CapabilityDescriptions { get; protected set; }
        /// <summary>
        /// If TRUE, hardware data compression is enabled.
        /// 0 = FALSE
        /// 1 = TRUE
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public System.UInt32? Compression { get; private set; }
        /// <summary>
        /// Free form string that indicates the algorithm or tool used by the device to support compression.
        /// <para />If it is not possible to describe the compression scheme (for example, because it is unknown), use the following:
        /// <para />"Unknown" to represent that it is unknown whether the device supports compression capabilities.
        /// <para />"Compressed" to represent that the device supports compression capabilities, but its compression scheme is either unknown or undisclosed.
        /// <para />"Not Compressed" to represent that the devices does not support compression capabilities.
        /// <para />This property is inherited from CIM_MediaAccessDevice.
        /// </summary>
        /// <remarks>dictionaryCompressionMethod</remarks>
        [libPlugNPlay.WMIProperty]
        public string CompressionMethod { get; protected set; }
        /// <summary>
        /// Default block size, in bytes, for the device. This property is inherited
        /// <para />For more information about using uint64 values in scripts, see Scripting in WMI.
        /// <para />This Property is inherited from CIM_MediaAccessDevice.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public System.UInt64? DefaultBlockSize { get; protected set; }
        /// <summary>
        /// If TRUE, the device supports hardware error correction.
        /// False (0)
        /// True (1)
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public System.UInt32? ECC { get; protected set; }
        /// <summary>
        /// Zone size for the end of tape (EOT) warning. This property is inherited from CIM_TapeDrive.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public System.UInt32? EOTWarningZoneSize { get; protected set; }
        /// <summary>
        /// Free-form string that describes the type(s) of error detection and correction supported by the device. This property is inherited from CIM_MediaAccessDevice.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public string ErrorMethodology { get; protected set; }
        /// <summary>
        /// High-order 32 bits of the device features flag.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public System.UInt32? FeaturesHigh { get; protected set; }
        /// <summary>
        /// Low-order 32 bits of the device features flag.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public System.UInt32? FeaturesLow { get; protected set; }
        /// <summary>
        /// Manufacturer's identifying name of the Windows CD ROM drive.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public string Id { get; protected set; }
        /// <summary>
        /// Maximum block size, in bytes, for media accessed by the device. This property is inherited from CIM_MediaAccessDevice.
        /// <para />For more information about using uint64 values in scripts, see Scripting in WMI.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public System.UInt64? MaxBlockSize { get; protected set; }
        /// <summary>
        /// Maximum size, in kilobytes, of media supported by the device. Kilobytes is interpreted as the number of bytes multiplied by 1000 (not the number of bytes multiplied by 1024). This property is inherited from CIM_MediaAccessDevice.
        /// <para />For more information about using uint64 values in scripts, see Scripting in WMI.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public System.UInt64? MaxMediaSize { get; protected set; }
        /// <summary>
        /// Maximum partition count for the tape drive. This property is inherited from CIM_TapeDrive.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public System.UInt32? MaxPartitionCount { get; protected set; }
        /// <summary>
        /// Media type used by (or accessed by) this device. In this case, it is set to "Tape Drive".
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public string MediaType { get; protected set; }
        /// <summary>
        /// Minimum block size, in bytes, for media accessed by the device. This property is inherited from CIM_MediaAccessDevice.
        /// <para />For more information about using uint64 values in scripts, see Scripting in WMI.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public System.UInt64? MinBlockSize { get; protected set; }
        /// <summary>
        /// If TRUE, the media access device needs cleaning. Manual or automatic cleaning is indicated in the Capabilities array property. This property is inherited from CIM_MediaAccessDevice.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public bool? NeedsCleaning { get; protected set; }
        /// <summary>
        /// When the media access device supports multiple individual media, this property defines the maximum number that can be supported or inserted. This property is inherited from CIM_MediaAccessDevice.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public System.UInt32? NumberOfMediaSupported { get; protected set; }
        /// <summary>
        /// Number of bytes inserted between blocks on a tape media. This property is inherited from CIM_TapeDrive.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public System.UInt32? Padding { get; protected set; }
        /// <summary>
        /// If TRUE, setmark reporting is enabled. Setmark reporting makes use of a specialized recorded element that does not contain user data. This recorded element is used to provide a segmentation scheme that is hierarchically superior to filemarks. Setmarks provide faster positioning on high-capacity tapes.
        /// 0 = FALSE
        /// 1 = TRUE
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public System.UInt32 ReportSetMarks { get; protected set; }
        #endregion
        #endregion

        public Win32_TapeDrive(System.Management.ManagementObject RefObj)
            : base(RefObj)
        { }

        protected new virtual void InitValues()
        {
            base.InitValues();
            //nop
        }
    }
}
