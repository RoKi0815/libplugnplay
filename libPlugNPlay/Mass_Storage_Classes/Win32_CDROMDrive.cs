﻿namespace libPlugNPlay
{
    /// <summary>
    /// The Win32_CDROMDrive WMI class represents a CD-ROM drive on a computer system running Windows.
    /// <para />Note  Be aware that the name of the drive does not correspond to the logical drive letter assigned to the device.
    /// </summary>
    /// <remarks>The Win32_CDROMDrive class is derived from CIM_CDROMDrive which derives from CIM_MediaAccessDevice. The CIM_MediaAccessDevice class derives from CIM_LogicalDevice. </remarks>
    /// <remarks>Namespace: Root\CIMV2</remarks>
    /// <remarks>MOF: CIMWin32.mof</remarks>
    /// <remarks>DLL: CIMWin32.dll</remarks>
    public class Win32_CDROMDrive : Win32_PnPEntity
    {
        public new const string UUID = "{8502C4B3-5FBB-11D2-AAC1-006008C78BC7}";
        public new const string WMI_Namespace = @"Root\CIMV2";
        public new const string WMI_ClassName = "Win32_CDROMDrive";
        public new static readonly System.Management.ManagementClass WMI_ClassReference = new System.Management.ManagementClass(@"\\.\" + WMI_Namespace + ":" + WMI_ClassName);

        #region methods
        #region from Win32_CDROMDrive
        /// <summary>
        /// Not implemented. To implement this method, see the Reset method in CIM_CDROMDrive for documentation.
        /// </summary>
        [libPlugNPlay.WMIMethod]
        public System.UInt32 Reset()
        {
            throw new System.NotImplementedException("This method is currently not implemented by WMI. To use this method, you must implement it in your own provider.");
        }
        /// <summary>
        /// Not implemented. To implement this method, see the SetPowerState method in CIM_CDROMDrive for documentation.
        /// </summary>
        [libPlugNPlay.WMIMethod]
        public System.UInt32 SetPowerState(libPlugNPlay.enumPowerStateValueMap PowerState, System.DateTime Time)
        {
            throw new System.NotImplementedException("This method is currently not implemented by WMI. To use this method, you must implement it in your own provider.");
        }
        #endregion
        #endregion

        #region properties
        #region from Win32_CDROMDrive
        #region inherited from Win32_PnPEntity (CIM_ManagedSystemElement, CIM_LogicalDevice)
        //enumAvailability? Availability;
        //string Caption;
        //enumConfigManagerErrorCode? ConfigManagerErrorCode;
        //bool? ConfigManagerUserConfig;
        //string CreationClassName;
        //string Description;
        //string DeviceID;
        //bool? ErrorCleared;
        //string ErrorDescription;
        //System.DateTime InstallDate;
        //System.UInt32? LastErrorCode;
        //string Manufacturer;
        //string Name;
        //string PNPDeviceID;
        //enumPowerManagementCapabilities[] PowerManagementCapabilities;
        //bool? PowerManagementSupported;
        //string Status;
        //enumStatusInfo? StatusInfo;
        //string SystemCreationClassName;
        //string SystemName;
        #endregion

        /// <summary>
        /// Capabilities of the media access device. This property is inherited from CIM_MediaAccessDevice.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public emunCapabilities[] Capabilities { get; protected set; }
        /// <summary>
        /// Array of free-form strings that provide detailed explanations for access device features indicated in the Capabilities array. Note, each array entry is related to the entry in the Capabilities array that is located at the same index. This property is inherited from CIM_MediaAccessDevice.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public string[] CapabilityDescriptions { get; protected set; }
        /// <summary>
        /// Free form string that indicates the algorithm or tool used by the device to support compression.
        /// <para />If it is not possible to describe the compression scheme (for example, because it is unknown), use the following:
        /// <para />"Unknown" to represent that it is unknown whether the device supports compression capabilities.
        /// <para />"Compressed" to represent that the device supports compression capabilities, but its compression scheme is either unknown or undisclosed.
        /// <para />"Not Compressed" to represent that the devices does not support compression capabilities.
        /// <para />This property is inherited from CIM_MediaAccessDevice.
        /// </summary>
        /// <remarks>dictionaryCompressionMethod</remarks>
        [libPlugNPlay.WMIProperty]
        public string CompressionMethod { get; protected set; }
        /// <summary>
        /// Default block size, in bytes, for the device. This property is inherited
        /// <para />For more information about using uint64 values in scripts, see Scripting in WMI.
        /// <para />This Property is inherited from CIM_MediaAccessDevice.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public System.UInt64? DefaultBlockSize { get; protected set; }
        /// <summary>
        /// Drive letter of the CD-ROM drive.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public string Drive { get; protected set; }
        /// <summary>
        /// If True, files can be accurately read from the CD device. This is achieved by reading a block of data twice and comparing the data against itself.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public bool? DriveIntegrity { get; protected set; }
        /// <summary>
        /// Free-form string that describes the type(s) of error detection and correction supported by the device. This property is inherited from CIM_MediaAccessDevice.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public string ErrorMethodology { get; protected set; }
        /// <summary>
        /// This property is obsolete. In place of this property, use FileSystemFlagsEx.
        /// </summary>
        [System.Obsolete("DEPRECATED, use DileSystemFlagsEx instead.")]
        [libPlugNPlay.WMIProperty]
        public System.UInt16? FileSystemFlags { get; protected set; }
        /// <summary>
        /// File system flags associated with the Windows CD-ROM drive. This parameter can be any combination of flags, but FS_FILE_COMPRESSION and FS_VOL_IS_COMPRESSED are mutually exclusive.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public enumFileSystemFlagsEx? FileSystemFlagsEx { get; protected set; }
        /// <summary>
        /// Drive letter that uniquely identifies this CD-ROM drive.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public string Id { get; protected set; }
        /// <summary>
        /// Maximum block size, in bytes, for media accessed by the device. This property is inherited from CIM_MediaAccessDevice.
        /// <para />For more information about using uint64 values in scripts, see Scripting in WMI.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public System.UInt64? MaxBlockSize { get; protected set; }
        /// <summary>
        /// Maximum length of a filename component supported by the Windows CD-ROM drive. A file name component the portion of a file name between backslashes. The value can be used to indicate that long names are supported by the specified file system. For example, for a FAT file system supporting long names, the function stores the value 255, rather than the previous 8.3 indicator. Long names can also be supported on systems that use the NTFS file system.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public System.UInt32? MaximumComponentLength { get; protected set; }
        /// <summary>
        /// Maximum size, in kilobytes, of media supported by the device. Kilobytes is interpreted as the number of bytes multiplied by 1000 (not the number of bytes multiplied by 1024). This property is inherited from CIM_MediaAccessDevice.
        /// <para />For more information about using uint64 values in scripts, see Scripting in WMI.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public System.UInt64? MaxMediaSize { get; protected set; }
        /// <summary>
        /// If True, a CD-ROM is in the drive.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public bool? MediaLoaded { get; protected set; }
        /// <summary>
        ///  Type of media that can be used or accessed by this device. Possible values are:
        ///  <para />"Random Access"
        ///  <para />"Supports Writing"
        ///  <para />"Removable Media"
        ///  <para />"CD-ROM"
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public string MediaType { get; protected set; }
        /// <summary>
        /// Firmware revision level that is assigned by the manufacturer.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public string MfrAssignedRevisionLevel { get; protected set; }
        /// <summary>
        /// Minimum block size, in bytes, for media accessed by the device. This property is inherited from CIM_MediaAccessDevice.
        /// <para />For more information about using uint64 values in scripts, see Scripting in WMI.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public System.UInt64? MinBlockSize { get; protected set; }
        /// <summary>
        /// If TRUE, the media access device needs cleaning. Manual or automatic cleaning is indicated in the Capabilities array property. This property is inherited from CIM_MediaAccessDevice.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public bool? NeedsCleaning { get; protected set; }
        /// <summary>
        /// When the media access device supports multiple individual media, this property defines the maximum number that can be supported or inserted. This property is inherited from CIM_MediaAccessDevice.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public System.UInt32? NumberOfMediaSupported { get; protected set; }
        /// <summary>
        /// Firmware revision level of the Windows CD-ROM drive.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public string RevisionLevel { get; protected set; }
        /// <summary>
        /// SCSI bus number for the disk drive.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public System.UInt32? SCSIBus { get; protected set; }
        /// <summary>
        /// SCSI logical unit number (LUN) of the disk drive. The LUN is used to designate which SCSI controller is being accessed in a system with more than one controller being used. The SCSI device identifier is similar, but is the designation for multiple devices on one controller.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public System.UInt16? SCSILogicalUnit { get; protected set; }
        /// <summary>
        /// SCSI port number of the disk drive.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public System.UInt16? SCSIPort { get; protected set; }
        /// <summary>
        /// SCSI identifier number of the Windows CD-ROM drive.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public System.UInt16? SCSITargetId { get; protected set; }
        /// <summary>
        /// Number supplied by the manufacturer that identifies the physical media.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public System.UInt16? SerialNumber { get; protected set; }
        /// <summary>
        /// Size of the disk drive.
        /// <para />For more information about using uint64 values in scripts, see Scripting in WMI.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public System.UInt64? Size { get; protected set; }
        /// <summary>
        /// Transfer rate of the CD-ROM drive. A value of -1 indicates that the rate cannot be determined. When this happens, the CD is not in the drive.
        /// </summary>
        /// <remarks>Units ("kilobytes per second")</remarks>
        [libPlugNPlay.WMIProperty]
        public System.Double? TransferRate { get; protected set; }
        /// <summary>
        /// Volume name of the Windows CD-ROM drive.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public string VolumeName { get; protected set; }
        /// <summary>
        /// Volume serial number of the media in the CD-ROM drive.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public string VolumeSerialNumber { get; protected set; }
        #endregion
        #endregion

        public Win32_CDROMDrive(System.Management.ManagementObject RefObj)
            : base(RefObj)
        { }

        protected new virtual void InitValues()
        {
            base.InitValues();
            //nop
        }
    }
}
