﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Management;

namespace libPlugNPlay
{
    /// <summary>
    /// The Win32_PnPDevice association WMI class relates a device (known to Configuration Manager as a PNPEntity) and the function it performs. Its function is represented by a subclass of the logical device that describes its use. For example, a Win32_Keyboard or Win32_DiskDrive instance. Both referenced objects represent the same underlying system device; changes to resource allocation on the PNPEntity side will effect the associated device.
    /// </summary>
    /// <remarks>Namespace: Root\CIMV2</remarks>
    /// <remarks>MOF: CIMWin32.mof</remarks>
    /// <remarks>DLL: CIMWin32.dll</remarks>
    public class Win32_PnPDevice
    {
        public new const string UUID = "{FE28FD96-C875-11d2-B352-00104BC97924}";
        public new const string WMI_Namespace = @"Root\CIMV2";
        public new const string WMI_ClassName = "Win32_PnPDevice";

        /// <summary>
        /// WMI Class Type of the Device
        /// </summary>
        public System.Type DeviceType;

        //\\SHINCHAN\root\CIMV2:Win32_SoundDevice.DeviceID="HDAUDIO\\FUNC_01&VEN_11D4&DEV_1988&SUBSYS_104381E1&REV_1004\\4&1423649C&0&0001"
        //private string rawstring_SameElement__CIM_LogicalDevice;
        private WMIObjectReference SameElement_ObjectReference;
        //\\SHINCHAN\root\cimv2:Win32_PnPEntity.DeviceID="HDAUDIO\\FUNC_01&VEN_11D4&DEV_1988&SUBSYS_104381E1&REV_1004\\4&1423649C&0&0001"
        //private string rawstring_SystemElement__Win32_PnPEntity;
        private WMIObjectReference SystemElement_ObjectReference;

        /// <summary>
        /// Reference to the CIM_LogicalDevice instance representing the logical device properties associated with the Plug and Play device.
        /// </summary>
        /// <remarks>-> CIM_LogicalDevice</remarks>
        [libPlugNPlay.WMIProperty]
        public System.Management.ManagementBaseObject SameElement { get; protected set; }
        /// <summary>
        /// Reference to the Win32_PnPEntity instance representing the Plug and Play device associated with the logical device.
        /// </summary>
        /// <remarks>-> Win32_PnPEntity (derived from CIM_Logicaldevice)</remarks>
        [libPlugNPlay.WMIProperty]
        public Win32_PnPEntity SystemElement { get; protected set; }

        static Win32_PnPDevice()
        {
            //Init SupportdTypesDictionary
        }

        /// <summary>
        /// Konstruktor. Erstellt ein Objekt mit Hilfe eines <see cref="System.Management.ManagementBaseObject"/> und legt alle Informationen in Eigenschaften ab.
        /// Eingebettete Objekte werden dabei aufgelöst.
        /// </summary>
        /// <param name="PnPDeviceObject"></param>
        public Win32_PnPDevice(System.Management.ManagementObject PnPDeviceObject)
        {
            //this.rawstring_SameElement__CIM_LogicalDevice = (string)PnPDeviceObject.GetPropertyValue("SameElement");
            //this.rawstring_SystemElement__Win32_PnPEntity = (string)PnPDeviceObject.GetPropertyValue("SystemElement");

            this.SameElement_ObjectReference = new WMIObjectReference((string)PnPDeviceObject.GetPropertyValue("SameElement"));
            this.SystemElement_ObjectReference = new WMIObjectReference((string)PnPDeviceObject.GetPropertyValue("SystemElement"));

            this.SameElement = this.SameElement_ObjectReference.GetWMIObject();
            
            //determine Type of Device
            

            //Get Object: SystemElement (first one found is used, theoretical there is only one found)
            foreach (ManagementObject device in new ManagementObjectSearcher("SELECT * FROM " + this.SameElement_ObjectReference.WMI_Class + " WHERE " + this.SameElement_ObjectReference.WMI_KeyProperties_string).Get())
            {
                //Console.WriteLine(device.ClassPath);
                //Console.WriteLine(device.Path);

                if (this.SameElement_ObjectReference.GetWMIClassType().Equals(typeof(Win32_PhysicalMedia)))
                {
                    this.SystemElement = new Win32_PhysicalMedia(device);
                }
                else if (this.SameElement_ObjectReference.GetWMIClassType().Equals(typeof(Win32_DiskDrive)))
                {
                    this.SystemElement = new Win32_DiskDrive(device);
                }
                else if (this.SameElement_ObjectReference.GetWMIClassType().Equals(typeof(Win32_CDROMDrive)))
                {
                    this.SystemElement = new Win32_CDROMDrive(device);
                }
                else if (this.SameElement_ObjectReference.GetWMIClassType().Equals(typeof(Win32_PnPEntity)))
                {
                    this.SystemElement = new Win32_PnPEntity(device);
                }
                else
                {
                    this.SystemElement = new Win32_PnPEntity(device);
                }
                break;
            }
        }

        private Win32_PnPEntity GetDevice<T>(System.Management.ManagementObject device) where T : Win32_PnPEntity, new()
        {
            return WMI.GetTypedObject<T>(device);
        }

        /// <summary>
        /// Get all Plug & Play devices of the local computer.
        /// </summary>
        /// <returns></returns>
        public static System.Collections.Generic.List<Win32_PnPDevice> GetPnPDevices()
        {
            List<Win32_PnPDevice> devices = new List<Win32_PnPDevice>();
            foreach (ManagementObject device in new ManagementObjectSearcher(@"SELECT * FROM Win32_PnPDevice").Get())
            {
                devices.Add(new Win32_PnPDevice(device));
            }
            return devices;
        }
    }
}
