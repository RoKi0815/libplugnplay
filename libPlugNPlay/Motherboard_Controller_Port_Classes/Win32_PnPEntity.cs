﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace libPlugNPlay
{
    /// <summary>
    /// The Win32_PnPEntity WMI class represents the properties of a Plug and Play device. Plug and Play entities are shown as entries in the Device Manager located in Control Panel.
    /// </summary>
    /// <remarks>The Win32_PnPEntity class is derived from CIM_LogicalDevice.</remarks>
    /// <remarks>Namespace: Root\CIMV2</remarks>
    /// <remarks>MOF: CIMWin32.mof</remarks>
    /// <remarks>DLL: CIMwin32.dll</remarks>
    public class Win32_PnPEntity
    {
        public const string UUID = "{FE28FD98-C875-11d2-B352-00104BC97924}";
        public const string WMI_Namespace = @"Root\CIMV2";
        public const string WMI_ClassName = "Win32_PnPEntity";
        public static readonly System.Management.ManagementClass WMI_ClassReference = new System.Management.ManagementClass(@"\\.\" + Win32_PnPEntity.WMI_Namespace + ":" + Win32_PnPEntity.WMI_ClassName);

        #region from Win32_PnpEntity
        #region Methods
        /// <summary>
        /// Disables this Plug and Play device.
        /// </summary>
        /// <param name="rebootNeeded">Whether a reboot is needed.</param>
        /// <returns></returns>
        [libPlugNPlay.WMIMethod]
        public UInt32 Disable(out bool rebootNeeded)
        {
            throw new System.NotImplementedException();
        }
        /// <summary>
        /// Enables this Plug and Play device.
        /// </summary>
        /// <param name="rebootNeeded">Wether a reboot is needed.</param>
        /// <returns></returns>
        [libPlugNPlay.WMIMethod]
        public UInt32 Enable(out bool rebootNeeded)
        {
            throw new System.NotImplementedException();
        }
        //GetDeviceProperties()
        //Reset()
        //SetpowerState()
        #endregion

        #region Properties
        /// <summary>
        /// Availability and status of the device. Inherited from CIM_LogicalDevice.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public enumAvailability? Availability { get; protected set; }
        /// <summary>
        /// Short description of the object. This property is inherited from CIM_ManagedSystemElement.
        /// </summary>
        /// <remarks>MaxLen (64)</remarks>
        [libPlugNPlay.WMIProperty]
        public string Caption { get; protected set; }
        /// <summary>
        /// Globally unique identifier (GUID) of this Plug and Play device.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public string ClassGuid { get; protected set; }
        /// <summary>
        /// A vendor-defined identification string that Setup uses to match a device to an INF file. A device can have a list of compatible IDs associated with it. The compatible IDs should be listed in order of decreasing suitability. If Setup cannot locate an INF file that matches one of a device's hardware IDs, it uses compatible IDs to locate an INF file. A compatible ID has the same format as a HardwareID. For more information, see Windows Driver Kit. 
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public string[] CompatibleID { get; protected set; }
        /// <summary>
        /// Win32 Configuration Manager error code.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public enumConfigManagerErrorCode? ConfigManagerErrorCode { get; protected set; }
        /// <summary>
        /// If TRUE, the device is using a user-defined configuration. This property is inherited from CIM_LogicalDevice.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public bool? ConfigManagerUserConfig { get; protected set; }
        /// <summary>
        /// Name of the first concrete class to appear in the inheritance chain used in the creation of an instance. When used with the other key properties of the class, the property allows all instances of this class and its subclasses to be uniquely identified. This property is inherited from CIM_LogicalDevice.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public string CreationClassName { get; protected set; }
        /// <summary>
        /// Description of the object. This property is inherited from CIM_ManagedSystemElement.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public string Description { get; protected set; }
        /// <summary>
        /// Identifier of the Plug and Play device. This property is inherited from CIM_LogicalDevice.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public string DeviceID { get; protected set; }
        /// <summary>
        /// If TRUE, the error reported in LastErrorCode is now cleared. This property is inherited from CIM_LogicalDevice.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public bool? ErrorCleared { get; protected set; }
        /// <summary>
        /// More information about the error recorded in LastErrorCode, and information about any corrective actions that may be taken. This property is inherited from CIM_LogicalDevice.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public string ErrorDescription { get; protected set; }
        /// <summary>
        /// A vendor-defined identification string that Setup uses to match a device to an INF file. Normally, a device has an associated list of hardware IDs. An exception is the 1394 bus driver, which does not use hardware IDs. The first hardware ID in the list should be the device ID. The remaining IDs should be listed in order of decreasing suitability.
        /// Hardware IDs appear in one the following formats:
        /// #   enumerator\enumerator-specific-device-ID
        ///     This is the most common format for individual PnP devices. An example of an enumerator is the BIOS or ISAPNP.
        /// #   *enumerator-specific ID
        ///     An asterisk (*) indicates use by more than one enumerator.
        /// #   device-class-specific ID
        ///     A custom format.
        /// Examples of Hardware IDs are:
        ///     root\*PNPOF08
        ///     PC\VEN_1000&DEV_001&SUBSYS_00000000&REV_02
        /// For more information, see the Windows Driver Kit.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public string[] HardwareID { get; protected set; }
        /// <summary>
        /// Date and time the object was installed. This property does not need a value to indicate that the object is installed. This property is inherited from CIM_ManagedSystemElement.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public DateTime? InstallDate { get; protected set; }
        /// <summary>
        /// Last error code reported by the logical device. This property is inherited from CIM_LogicalDevice.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public UInt32? LastErrorCode { get; protected set; }
        /// <summary>
        /// Name of the manufacturer of the Plug and Play device.
        /// Example: "Acme"
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public string Manufacturer { get; protected set; }
        /// <summary>
        /// Label by which the object is known. When subclassed, the property can be overridden to be a key property. This property is inherited from CIM_ManagedSystemElement.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public string Name { get; protected set; }
        /// <summary>
        /// Warning  This property, despite being listed in the MOF file, does not actually exist in the class. The property is described here only for the sake of completeness, and to clarify the MOF file itself.
        /// <para />The name of the type of this Plug and Play device.
        /// </summary>
        /// <remarks>Windows Server 2012 R2, Windows 8.1, Windows Server 2012, Windows 8, Windows Server 2008 R2, Windows 7, Windows Server 2008, and Windows Vista:  This property is not in the MOF file.</remarks>
        [libPlugNPlay.WMIProperty, libPlugNPlay.WMINotExisting]
        public string PNPClass { get; protected set; }
        /// <summary>
        /// Windows Plug and Play device identifier of the logical device. This property is inherited from CIM_LogicalDevice.
        /// Example: "*PNP030b"
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public string PNPDeviceID { get; protected set; }
        /// <summary>
        /// Not implemented.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public enumPowerManagementCapabilities[] PowerManagementCapabilities { get; protected set; }
        /// <summary>
        /// Not implemented.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public bool? PowerManagementSupported { get; protected set; }
        /// <summary>
        /// Whether this Plug and Play device is currently in the system.
        /// </summary>
        /// <remarks>Windows Server 2012 R2, Windows 8.1, Windows Server 2012, Windows 8, Windows Server 2008 R2, Windows 7, Windows Server 2008, and Windows Vista:  This property is not supported.</remarks>
        [libPlugNPlay.WMIProperty, libPlugNPlay.WMINotExisting]
        public bool? Present { get; protected set; }
        /// <summary>
        /// Name of the service that supports this Plug and Play device. For more information, see <see cref="Win32_SystemDriverPnPEntity"/>.
        /// Example: "atapi"
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public string Service { get; protected set; }
        /// <summary>
        /// Current status of the object. Various operational and nonoperational statuses can be defined. Operational statuses include: "OK", "Degraded", and "Pred Fail" (an element, such as a SMART-enabled hard disk drive, may be functioning properly but predicting a failure in the near future). Nonoperational statuses include: "Error", "Starting", "Stopping", and "Service". The latter, "Service", could apply during mirror-resilvering of a disk, reload of a user permissions list, or other administrative work. Not all such work is online, yet the managed element is neither "OK" nor in one of the other states. This property is inherited from CIM_ManagedSystemElement.
        /// </summary>
        /// <remarks>MaxLen(10), <see cref="dictionaryStatus"/></remarks>
        [libPlugNPlay.WMIProperty]
        public string Status { get; protected set; }
        /// <summary>
        /// State of the logical device. If this property does not apply to the logical device, the value 5 (Not Applicable) should be used. This property is inherited from CIM_LogicalDevice.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public enumStatusInfo? StatusInfo { get; protected set; }
        /// <summary>
        /// Value of the scoping computer's CreationClassName property. This property is inherited from CIM_LogicalDevice.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public string SystemCreationClassName { get; protected set; }
        /// <summary>
        /// Name of the scoping system. This property is inherited from CIM_LogicalDevice.
        /// </summary>
        [libPlugNPlay.WMIProperty]
        public string SystemName { get; protected set; }
        #endregion
        #endregion

        private System.Collections.Generic.Dictionary<string, object> Properties = new System.Collections.Generic.Dictionary<string, object>();

        /// <summary>
        /// Konstruktor. Erstellt das Objekt mit Hilfe eines <see cref="System.Management.ManagementBaseObject"/> und legt alle Informationen in Eigenschaften ab.
        /// </summary>
        /// <param name="RefObj">Informationsguelle</param>
        public Win32_PnPEntity(System.Management.ManagementObject RefObj)
        {
            this.InitValues();
            Win32_PnPEntity.FillPropertiesFromMBO(this, RefObj);
        }

        /// <summary>
        /// Optional default value initialization.
        /// </summary>
        protected virtual void InitValues()
        {
            //nop
        }

        /// <summary>
        /// Fill a given property of the given target object with the information of the source object.
        /// </summary>
        /// <param name="TargetObject"></param>
        /// <param name="TargetProperty"></param>
        /// <param name="Source"></param>
        private static void SetPropertyFromMBO(Win32_PnPEntity TargetObject, System.Reflection.PropertyInfo TargetProperty, System.Management.ManagementBaseObject Source)
        {
            try
            {
                object val = null;
                if (null != Source.GetPropertyValue(TargetProperty.Name))
                {
                    if (TargetProperty.PropertyType.IsNullableEnum())
                    {
                        val = Enum.Parse(Nullable.GetUnderlyingType(TargetProperty.PropertyType), (string)Convert.ChangeType(Convert.ChangeType(Source.GetPropertyValue(TargetProperty.Name), Enum.GetUnderlyingType(Nullable.GetUnderlyingType(TargetProperty.PropertyType))), typeof(string)));
                    }
                    else
                    {
                        val = Source.GetPropertyValue(TargetProperty.Name);
                    }
                }
                //Targetproperty has no default value other than "null" set?
                if (null == TargetProperty.GetValue(TargetObject, null))
                {
                    TargetProperty.SetValue(TargetObject, val, null);
                }
            }
            catch (Exception ex)
            {
                if (ex is System.NullReferenceException)
                {
                    //Eigenschaftswert nicht beschrieben (in abgeleiteter downgecasteter Klasse nicht vorhanden) -> null setzen
                    //Targetproperty has no default value other than "null" set?
                    if (null == TargetProperty.GetValue(TargetObject, null))
                    {
                        TargetProperty.SetValue(TargetObject, null, null);
                    }
                }
                else if (ex is System.Management.ManagementException)
                {
                    if (((System.Management.ManagementException)ex).ErrorCode == System.Management.ManagementStatus.NotFound)
                    {
                        Console.WriteLine(TargetProperty.Name + " was not found");
                        //Eigenschaft nicht vorhanden -> null setzen
                        TargetProperty.SetValue(TargetObject, null, null);
                    }
                    else
                    {
                        throw;
                    }
                }
                else
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Fill all property of the given target object with the information of the source object.
        /// </summary>
        /// <param name="TargetObject"></param>
        /// <param name="Source"></param>
        private static void FillPropertiesFromMBO(Win32_PnPEntity TargetObject, System.Management.ManagementObject Source)
        {
            //für jede vorhandene Property im Ziel mit Attribut <see cref="WMIPropertyAttribute">
            foreach (System.Reflection.PropertyInfo pi in TargetObject.GetType().GetProperties(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance).Where(pi => pi.HasAttribute<libPlugNPlay.WMIPropertyAttribute>()))
            {
                Win32_PnPEntity.SetPropertyFromMBO(TargetObject, pi, Source);
            }
        }

        /// <summary>
        /// Get all Plug & Play entities of the local computer.
        /// </summary>
        /// <returns></returns>
        public static System.Collections.Generic.List<Win32_PnPEntity> GetPnPEntities()
        {
            List<Win32_PnPEntity> entities = new List<Win32_PnPEntity>();
            foreach (System.Management.ManagementObject entity in new System.Management.ManagementObjectSearcher(@"SELECT * FROM Win32_PnPEntity").Get())
            {
                entities.Add(new Win32_PnPEntity(entity));
            }
            return entities;
        }

        #region Operator
        /// <summary>
        /// Implizite Typumwandlung. Von <see cref="System.Management.ManagementBaseObject"/> in <see cref="Win32_PnPEntity"/>.
        /// </summary>
        /// <param name="src">Quellobjekt</param>
        /// <returns>Zielobjekt</returns>
        public static implicit operator Win32_PnPEntity(System.Management.ManagementObject src)
        {
            return new Win32_PnPEntity(src);
        }
        #endregion
    }
}
